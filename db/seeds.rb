puts "Inside seed generation"

puts "---- ERASED ALL SUBSCRITPIONS RELATED MODELS ----"
SubscriptionType.destroy_all
SubscriptionItem.destroy_all
puts "----------------------------------------"

puts "---- GENERATE SUBSCRITPIONS TYPES ----"
subs = [{name: "Portails - diffusion", slug_name: "portails_diffusion", required: true, input_value: 1, svg_name: "megaphone"},
        {name: "Vitrine", slug_name: "vitrine", required: false, input_value: 2, svg_name: "postcard"},
        {name: "Logiciels", slug_name: "logiciels", required: true, input_value: 3, svg_name: "window"},
        {name: "Réseau", slug_name: "reseau", required: false, input_value: 4, svg_name: "share-alt"},
        {name: "Franchise", slug_name: "franchise", required: true, input_value: 5, svg_name: "star"},
        {name: "Garantie - Assurances", slug_name: "garantie_financieres_assurances", required: true, input_value: 6, svg_name: "umbrella"},
        {name: "Télécom", slug_name: "telecom", required: true, input_value: 7, svg_name: "unicon-phone"},
        {name: "Locaux - véhicules", slug_name: "locaux_vehicules", required: false, input_value: 8, svg_name: "store-alt"}]
subs.each do |hashed_values|
  @sub = SubscriptionType.new(hashed_values)
  if @sub.save
    puts "Sub successfully created with name: #{@sub.name}"
  else
    puts "Sub creation failed due to: #{@sub.errors.full_messages}"
  end
end
puts "--------------------------------------"


@sub1 = SubscriptionType.find_by(input_value: 1)
@sub2 = SubscriptionType.find_by(input_value: 2)
@sub3 = SubscriptionType.find_by(input_value: 3)
@sub4 = SubscriptionType.find_by(input_value: 4)
@sub5 = SubscriptionType.find_by(input_value: 5)
@sub6 = SubscriptionType.find_by(input_value: 6)
@sub7 = SubscriptionType.find_by(input_value: 7)
@sub8 = SubscriptionType.find_by(input_value: 8)



puts "---- GENERATE SUBS ITEMS FOR: #{@sub1.name} ----"
items = [{name: "Se Loger", slug_name: "seloger", subscription_type: @sub1, svg_name: "seloger"},
         {name: "MeilleursAgents", slug_name: "meilleurs_agents", subscription_type: @sub1},
         {name: "Bien'Ici", slug_name: "bienici", subscription_type: @sub1},
         {name: "Le Bon Coin", slug_name: "le_bon_coin", subscription_type: @sub1},
         {name: "Logic'Immo", slug_name: "logic_immo", subscription_type: @sub1},
         {name: "Le Figaro Immo", slug_name: "le_figaro_immo", subscription_type: @sub1},
         {name: "Ouest France Immo", slug_name: "ouest_france_immo", subscription_type: @sub1},
         {name: "Belles demeures", slug_name: "belles_demeures", subscription_type: @sub1},
         {name: "Paru-Vendu", slug_name: "paru_vendu", subscription_type: @sub1},
         {name: "A vendre a louer", slug_name: "a_vendre_a_louer", subscription_type: @sub1},
         {name: "Publicité papier", slug_name: "publicite_papier", subscription_type: @sub1}]
items.each do |item_values|
  @item = SubscriptionItem.new(item_values)
  if @item.save
    puts "Item successfully created with name: #{@item.name}"
  else
    puts "Item creation failed due to: #{@item.errors.full_messages}"
  end
end
puts "------------------------------------------------"



puts "---- GENERATE SUBS ITEMS FOR: #{@sub2.name} ----"
items = [{name: "Vitrilia", slug_name: "vitrilia", subscription_type: @sub2},
         {name: "Kimex", slug_name: "kimex", subscription_type: @sub2},
         {name: "Digeemo", slug_name: "digeemo", subscription_type: @sub2},
         {name: "VitrineMedia", slug_name: "vitrine_media", subscription_type: @sub2}]
items.each do |item_values|
  @item = SubscriptionItem.new(item_values)
  if @item.save
    puts "Item successfully created with name: #{@item.name}"
  else
    puts "Item creation failed due to: #{@item.errors.full_messages}"
  end
end
puts "------------------------------------------------"




puts "---- GENERATE SUBS ITEMS FOR: #{@sub3.name} ----"
items = [{name: "ImmoFacile", slug_name: "immo_facile", subscription_type: @sub3},
         {name: "Périclès", slug_name: "pericles", subscription_type: @sub3},
         {name: "Hektor", slug_name: "hektor", subscription_type: @sub3},
         {name: "Netty", slug_name: "netty", subscription_type: @sub3},
         {name: "Crypto", slug_name: "crypto", subscription_type: @sub3},
         {name: "AC3", slug_name: "ac3", subscription_type: @sub3},
         {name: "AdaptImmo", slug_name: "adapt_immo", subscription_type: @sub3},
         {name: "CityScann", slug_name: "city_scann", subscription_type: @sub3},
         {name: "Transactimmo", slug_name: "transactimmo", subscription_type: @sub3},
         {name: "Apimo", slug_name: "Apimo", subscription_type: @sub3},
         {name: "Consortium immobilier", slug_name: "consortium_immobilier", subscription_type: @sub3},
         {name: "Roadcom", slug_name: "roadcom", subscription_type: @sub3},
         {name: "Eurodotnet", slug_name: "eurodotnet", subscription_type: @sub3},
         {name: "H2i", slug_name: "h2i", subscription_type: @sub3},
         {name: "Ubiflow", slug_name: "ubiflow", subscription_type: @sub3},
         {name: "Homedata", slug_name: "homedata", subscription_type: @sub3},
         {name: "Jestimmo", slug_name: "jestimmo", subscription_type: @sub3},
         {name: "Alveen", slug_name: "alveen", subscription_type: @sub3},
         {name: "My Notary", slug_name: "my_notary", subscription_type: @sub3},
         {name: "Aristid", slug_name: "aristid", subscription_type: @sub3},
         {name: "Edmond", slug_name: "edmond", subscription_type: @sub3},
         {name: "Bientôt Ma Loc", slug_name: "bientot_ma_loc", subscription_type: @sub3},
         {name: "Ma Lab", slug_name: "malab", subscription_type: @sub3},
         {name: "Quai des notaires", slug_name: "quai_des_notaires", subscription_type: @sub3}]
items.each do |item_values|
  @item = SubscriptionItem.new(item_values)
  if @item.save
    puts "Item successfully created with name: #{@item.name}"
  else
    puts "Item creation failed due to: #{@item.errors.full_messages}"
  end
end
puts "------------------------------------------------"





puts "---- GENERATE SUBS ITEMS FOR: #{@sub4.name} ----"
items = [{name: "FNAIM", slug_name: "fnaim", subscription_type: @sub4},
         {name: "SNPI", slug_name: "snpi", subscription_type: @sub4},
         {name: "AMEPI", slug_name: "amepi", subscription_type: @sub4}]
items.each do |item_values|
  @item = SubscriptionItem.new(item_values)
  if @item.save
    puts "Item successfully created with name: #{@item.name}"
  else
    puts "Item creation failed due to: #{@item.errors.full_messages}"
  end
end
puts "------------------------------------------------"





puts "---- GENERATE SUBS ITEMS FOR: #{@sub5.name} ----"
items = [{name: "Laforêt", slug_name: "laforet", subscription_type: @sub5},
         {name: "Guy Hoquet", slug_name: "guy_hoquet", subscription_type: @sub5},
         {name: "Era", slug_name: "era", subscription_type: @sub5},
         {name: "Orpi", slug_name: "orpi", subscription_type: @sub5},
         {name: "L'Adresse", slug_name: "ladresse", subscription_type: @sub5},
         {name: "Keller Williams", slug_name: "keller_williams", subscription_type: @sub5},
         {name: "Nestenn", slug_name: "nestenn", subscription_type: @sub5},
         {name: "Century 21", slug_name: "century_21", subscription_type: @sub5},
         {name: "Stéphane Plaza", slug_name: "stephane_plaza", subscription_type: @sub5},
         {name: "Espaces Atypiques", slug_name: "espaces_atypiques", subscription_type: @sub5},
         {name: "Bourse de l'immobilier", slug_name: "bourse_immobilier", subscription_type: @sub5},
         {name: "Côté Particuliers", slug_name: "cote_particuliers", subscription_type: @sub5},
         {name: "Keymex", slug_name: "keymex", subscription_type: @sub5},
         {name: "CIMM Immobilier", slug_name: "cimm_immobilier", subscription_type: @sub5},
         {name: "Cabinet Bedin", slug_name: "cabinet_bedin", subscription_type: @sub5},
         {name: "Indépendant", slug_name: "independant", subscription_type: @sub5},
         {name: "Intégré", slug_name: "integre", subscription_type: @sub5}]
items.each do |item_values|
  @item = SubscriptionItem.new(item_values)
  if @item.save
    puts "Item successfully created with name: #{@item.name}"
  else
    puts "Item creation failed due to: #{@item.errors.full_messages}"
  end
end
puts "------------------------------------------------"






puts "---- GENERATE SUBS ITEMS FOR: #{@sub6.name} ----"
items = [{name: "Galian", slug_name: "galian", subscription_type: @sub6},
         {name: "Socaf", slug_name: "socaf", subscription_type: @sub6},
         {name: "Verspieren", slug_name: "verspieren", subscription_type: @sub6},
         {name: "MMA", slug_name: "mma", subscription_type: @sub6},
         {name: "AXA", slug_name: "axa", subscription_type: @sub6},
         {name: "Macif", slug_name: "macif", subscription_type: @sub6},
         {name: "Aviva", slug_name: "aviva", subscription_type: @sub6}]
items.each do |item_values|
  @item = SubscriptionItem.new(item_values)
  if @item.save
    puts "Item successfully created with name: #{@item.name}"
  else
    puts "Item creation failed due to: #{@item.errors.full_messages}"
  end
end
puts "------------------------------------------------"




puts "---- GENERATE SUBS ITEMS FOR: #{@sub7.name} ----"
items = [{name: "Abonnement mobile", slug_name: "abonnement_mobile", subscription_type: @sub7},
         {name: "Abonnement internet", slug_name: "abonnement_internet", subscription_type: @sub7},
         {name: "Photocopie - Télécopie", slug_name: "photocopie_telecopie", subscription_type: @sub7},
         {name: "Téléphone fixe", slug_name: "telephone_fixe", subscription_type: @sub7}]
items.each do |item_values|
  @item = SubscriptionItem.new(item_values)
  if @item.save
    puts "Item successfully created with name: #{@item.name}"
  else
    puts "Item creation failed due to: #{@item.errors.full_messages}"
  end
end
puts "------------------------------------------------"




puts "---- GENERATE SUBS ITEMS FOR: #{@sub8.name} ----"
items = [{name: "Voiture(leasing...)", slug_name: "voiture", subscription_type: @sub8},
         {name: "Assurance voiture", slug_name: "assurance_voiture", subscription_type: @sub8}]
items.each do |item_values|
  @item = SubscriptionItem.new(item_values)
  if @item.save
    puts "Item successfully created with name: #{@item.name}"
  else
    puts "Item creation failed due to: #{@item.errors.full_messages}"
  end
end
puts "------------------------------------------------"





