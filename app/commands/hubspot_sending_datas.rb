class HubspotSendingDatas
  prepend SimpleCommand

  def initialize(user)
    @user = user
    @company = user.companies.last
    @guid = "386ecb7d-ac0b-45d2-9d53-287819b33a5e"
    @portal_id = "6649850"
    @api_key = Rails.application.credentials.hubspot_api_key
  end

  def call
    sending_datas
  end

  private

  def sending_datas
    post_url = "https://forms.hubspot.com/uploads/form/v2/#{@portal_id}/#{@guid}"
    form_params = {"email" => @user.email, "firstname" => @user.firstname, "lastname" => @user.lastname, "phone" => @user.phone, "company" => @company.name, "city" => @company.city, "zip" => @company.postal_code, "produit" => "Edmond", "source" => "Plateforme Edmond", "address" => @company.full_address}
    request = RestClient.post(post_url,form_params,{content_type: "application/x-www-form-urlencoded"})
  end

end
