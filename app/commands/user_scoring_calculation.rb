class UserScoringCalculation
  prepend SimpleCommand

  def initialize(user)
    @user = user
    @company = @user.companies.last
    @datas = []
    @classes = ["bg-danger", "bg-danger", "bg-warning", "bg-secondary", "bg-info", "bg-success"]
    if @user.user_scoring?
      @scoring = @user.user_scoring
      puts @scoring.inspect
    else
      @scoring = UserScoring.new(user: @user)
      if @scoring.save
        puts "Scoring saved !!"
      else
        puts "Scoring not saved !!"
      end
    end
  end

  def call
    calculate_contractor_score
    calculate_balance_sheet_score
    calculate_treasury_score
    calculate_activity_score
    calculate_quality_score
    calculate_market_score
    return @datas
  end

  private

  def calculate_contractor_score
    return if @company.activity_start_date == nil
    return if @company.juridical_nature == nil

    start_date = @company.activity_start_date
    elapsed_time = ((Date.today - start_date).to_f / 365.0).round(2)
    company_type = @company[:juridical_nature]
    elapsed_time_coef = 2
    juridical_nature_coef = 2

    if elapsed_time < 1.0
      score = 1
      elapsed_time_score = 1 * elapsed_time_coef
    elsif elapsed_time < 2.0
      score = 2
      elapsed_time_score = 2 * elapsed_time_coef
    elsif elapsed_time > 2.0 && elapsed_time < 5.0
      score = 3
      elapsed_time_score = 3 * elapsed_time_coef
    elsif elapsed_time >= 5.0 && elapsed_time < 10.0
      score = 4
      elapsed_time_score = 4 * elapsed_time_coef
    elsif elapsed_time >= 10.0
      score = 5
      elapsed_time_score = 5 * elapsed_time_coef
    else
      score = 0
    end


    if company_type == "5710" || company_type == "5720"
      value = 5 * juridical_nature_coef
    elsif company_type == "5498" || company_type == "5499"
      value = 4 * juridical_nature_coef
    elsif company_type == "1000"
      value = 1 * juridical_nature_coef
    else
      value = 0
    end

    puts "€€€€€€€€€€€€€€€"
    puts elapsed_time_score.inspect
    puts value.inspect
    puts "€€€€€€€€€€€€€€€"

    if score == 0
      width = 0
    else
      width = 20 * score
    end
    @datas << {name: "Entrepreneur", score: score, class: "#{@classes[score]}", width: width}
  end

  def calculate_balance_sheet_score
    score = 2
    if score == 0
      width = 0
    else
      width = 20 * score
    end
    @datas << {name: "Bilan", score: score, class: "#{@classes[score]}", width: width}
  end

  def calculate_treasury_score
    score = 5
    if score == 0
      width = 0
    else
      width = 20 * score
    end
    @datas << {name: "Trésorerie", score: score, class: "#{@classes[score]}", width: width}
  end

  def calculate_activity_score
    score = 1
    if score == 0
      width = 0
    else
      width = 20 * score
    end
    @datas << {name: "Activité", score: score, class: "#{@classes[score]}", width: width}
  end

  def calculate_quality_score
    score = 0
    if score == 0
      width = 0
    else
      width = 20 * score
    end
    @datas << {name: "360° quality", score: score, class: "#{@classes[score]}", width: width}
  end

  def calculate_market_score
    score = 3
    if score == 0
      width = 0
    else
      width = 20 * score
    end
    @datas << {name: "Marché", score: score, class: "#{@classes[score]}", width: width}
  end

end
