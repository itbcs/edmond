class GenerateManagementDatas
  prepend SimpleCommand

  def initialize(company,computation_window)
    if computation_window == :fullyear
      @start_month = Date.today.beginning_of_year
      @end_month = Date.today.end_of_year
      @date_range = DateRange.new(@start_month,@end_month)
    elsif computation_window == :halfyear
      @start_month = Date.today.beginning_of_year
      @end_month = Date.today.beginning_of_year + 6.months
      @date_range = DateRange.new(@start_month,@end_month)
    end
    @company = company
    @datas = {fees: [], current_month_unsigned_fees: [], costs: [], current_year_subs: [], current_year_subs_sum: 0, cashed: 0, uncashed: 0, labels: [], current_balance: 0, target_balance: 0}
  end

  def call
    generate_labels
    calculate_signed_fees
    calculate_unsigned_fees
    calculate_costs
    calculate_subscriptions
    calculate_cashed_and_uncashed_costs
    calculate_balance
  end

  private

  def generate_labels
    @datas[:labels] = @date_range.range_list
  end

  def calculate_signed_fees
    if @company.compromises?
      (@start_month..@end_month).to_a.map{|date| [date.month,date.year]}.uniq.each do |date|
        month = date.first.to_i
        year = date.last.to_i
        if month == Date.today.month.to_i
          month_sum = @company.compromises.where(authentic_act_signature_date_year: year).where(authentic_act_signature_date_month: month).select{|comp| comp if comp[:status] == 0}.map{|comp| comp.df_fees.to_i}.sum
        else
          month_sum = @company.compromises.where(authentic_act_signature_date_year: year).where(authentic_act_signature_date_month: month).map{|comp| comp.df_fees.to_i}.sum
        end

        if month_sum != 0
          @datas[:fees] << month_sum
        else
          @datas[:fees] << 0
        end
        puts "#{month_sum} for #{month}-#{year}"
      end
    else
      (@start_month..@end_month).to_a.map{|date| [date.month,date.year]}.uniq.each do |date|
        @datas[:fees] << 0
      end
    end
    return @datas
  end

  def calculate_unsigned_fees
    if @company.compromises?
      (@start_month..@end_month).to_a.map{|date| [date.month,date.year]}.uniq.each do |date|
        month = date.first.to_i
        year = date.last.to_i
        if month == Date.today.month.to_i
          month_sum = @company.compromises.where(authentic_act_signature_date_year: year).where(authentic_act_signature_date_month: month).select{|comp| comp if comp[:status] == 1}.map{|comp| comp.df_fees.to_i}.sum
          if month_sum != 0
            @datas[:current_month_unsigned_fees] << month_sum
          else
            @datas[:current_month_unsigned_fees] << ""
          end
        else
          @datas[:current_month_unsigned_fees] << ""
        end
        puts "#{month_sum} for #{month}-#{year}"
      end
    else
      (@start_month..@end_month).to_a.map{|date| [date.month,date.year]}.uniq.each do |date|
        @datas[:current_month_unsigned_fees] << ""
      end
    end
    return @datas
  end

  def calculate_costs
    if @company.costs?
      (@start_month..@end_month).to_a.map{|date| [date.month,date.year]}.uniq.each do |date|
        month = date.first.to_i
        year = date.last.to_i
        month_sum = @company.costs.where(year: year).where(month: month).map{|cost| cost.total}.sum
        @datas[:costs] << month_sum
        puts "#{month_sum} for #{month}-#{year}"
      end
    else
      (@start_month..@end_month).to_a.map{|date| [date.month,date.year]}.uniq.each do |date|
        @datas[:costs] << 0
      end
    end
    return @datas
  end

  def calculate_subscriptions
    @subs_total = []
    if @company.subscriptions?
      @total = @company.subscriptions_amount_sum

      (@start_month..@end_month).to_a.map{|date| [date.month,date.year]}.uniq.each do |date|
        month = date.first.to_i
        year = date.last.to_i
        parsed_date = Date.parse("#{year}/#{month}")
        month_sum = @company.monthly_subs.where(:start_date.lte => parsed_date, :end_date.gte => parsed_date).map{|item| item.int_amount.to_i}.sum

        if month_sum
          @subs_total << month_sum
        else
          @subs_total << 0
        end
      end
    else
      (@start_month..@end_month).to_a.map{|date| [date.month,date.year]}.uniq.each do |date|
        @subs_total << 0
      end
    end

    @datas[:current_year_subs] = @subs_total
    @datas[:current_year_subs_sum] = @subs_total.sum
    @datas[:costs] = [@datas[:costs],@subs_total].transpose.map{|x| x.sum}


    @company.annual_subs.each do |sub|
      index = (sub.start_date.month.to_i - 1)
      if @datas[:costs][index]
        @datas[:costs][index] += sub.int_amount
      end
      @datas[:current_year_subs_sum] += sub.int_amount
    end


    return @datas
  end

  def calculate_cashed_and_uncashed_costs
    current_month = Date.today.month.to_i - 1
    costs = @datas[:costs]
    cashed_costs = costs[0..current_month].sum
    uncashed_costs = costs.drop(current_month + 1).sum
    @datas[:cashed] = cashed_costs != 0 ? cashed_costs : 0
    @datas[:uncashed] = uncashed_costs != 0 ? uncashed_costs : 0
    return @datas
  end

  def calculate_balance
    @datas[:current_balance] = (@company.signed_compromises_fees_sum.to_i - @datas[:cashed].to_i)
    @datas[:target_balance] = (@company.unsigned_compromises_fees_sum.to_i - @datas[:uncashed].to_i)
    return @datas
  end

end
