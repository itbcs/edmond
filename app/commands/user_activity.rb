class UserActivity
  prepend SimpleCommand

  def initialize(company)
    @company = company
    @datas = {}
  end

  def call
    activity
  end

  private

  def activity
    if @company.compromises?
      @datas[:compromise] = @company.compromises.last
    else
      @datas[:compromise] = nil
    end

    if @company.costs?
      @datas[:cost] = @company.costs.last
    else
      @datas[:cost] = nil
    end

    if @company.subscriptions?
      @datas[:subscription] = @company.subscriptions.last
    else
      @datas[:subscription] = nil
    end

    if @company.financing_requests?
      @datas[:financing_request] = @company.financing_requests.last
    else
      @datas[:financing_request] = nil
    end

    return @datas
  end

end
