class InitiateStripeAccount
  prepend SimpleCommand

  def initialize(user,success_url,cancel_url)
    @user = user
    @success_url = (success_url + "?session_id={CHECKOUT_SESSION_ID}")
    @cancel_url = cancel_url
    @session_ids = {}
    @sessions = {}
    @plans = []
    @taxes = []
  end

  def call
    list_all_taxes
    list_all_plans
    new_stripe_customer
    return @plans
  end

  private

  def list_all_taxes
    lists = Stripe::TaxRate.list
    lists.as_json["data"].each do |list|
      datas = {}
      list_id = list["id"]
      list_active = list["active"]
      list_desc = list["description"]
      list_name = list["display_name"]
      list_inclusive = list["inclusive"]
      list_jurisdiction = list["jurisdiction"]
      list_livemode = list["livemode"]
      list_percent = list["percentage"]
      datas[:id] = list_id
      datas[:active] = list_active
      datas[:desc] = list_desc
      datas[:name] = list_name
      datas[:inclusive] = list_inclusive
      datas[:jurisdiction] = list_jurisdiction
      datas[:livemode] = list_livemode
      datas[:percent] = list_percent
      @taxes << datas
    end
    puts @taxes.inspect
  end

  def list_all_plans
    plans = Stripe::Plan.list
    plans.as_json["data"].each do |plan|
      plan_id = plan["id"]
      plan_name = plan["metadata"]["rails_name"]
      plan_amount = plan["metadata"]["rails_amount"]
      plan_description = plan["metadata"]["rails_description"]
      html_class = plan["metadata"]["rails_html_class"]
      object = {id: plan_id, name: plan_name, amount: plan_amount, description: plan_description, class: html_class}
      @plans << object
    end
  end

  def new_stripe_customer
    @plans.each do |plan|
      if @user.stripe_subscription_status == "canceled"
        plan[:session_options] = {locale: "fr", client_reference_id: @user.id.to_s, customer_email: @user.email, payment_method_types: ['card'], subscription_data: {items: [{plan: plan[:id]}], default_tax_rates: [@taxes.last[:id]]}, success_url: @success_url, cancel_url: @cancel_url}
      else
        plan[:session_options] = {locale: "fr", client_reference_id: @user.id.to_s, customer_email: @user.email, payment_method_types: ['card'], subscription_data: {items: [{plan: plan[:id]}], trial_from_plan: true, default_tax_rates: [@taxes.last[:id]]}, success_url: @success_url, cancel_url: @cancel_url}
      end
    end
    @plans.each do |plan|
      session_id = Stripe::Checkout::Session.create(plan[:session_options])["id"]
      plan[:session] = session_id
    end
  end

end
