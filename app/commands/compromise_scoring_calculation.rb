class CompromiseScoringCalculation
  prepend SimpleCommand

  def initialize(compromise)
    @compromise = compromise
    @datas = []
    if @compromise.compromise_scoring?
      @scoring = @compromise.compromise_scoring
    else
      @scoring = CompromiseScoring.new(compromise: @compromise)
      if @scoring.save
        puts "Scoring success"
      else
        puts "Scoring failed"
      end
    end
  end

  def call
    calculate_contract_type
    return @datas
  end

  private

  def calculate_contract_type
    puts @compromise.inspect
    if @compromise.contract_type
      if @compromise[:contract_type] == 0
        @datas << {name: "Type de contrat", score: 5}
      elsif @compromise[:contract_type] == 1
        @datas << {name: "Type de contrat", score: 2}
      end
    end
  end

end
