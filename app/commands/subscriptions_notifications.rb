class SubscriptionsNotifications
  prepend SimpleCommand

  def initialize
    @current_date = Date.today
  end

  def call
    get_ninety_days_subs
    get_sixty_days_subs
    get_thirty_days_subs
  end

  private

  def get_ninety_days_subs
    @subs = Subscription.ninty_unnotified
    @nineties_subs = @subs.where(end_date: {"$gt": @current_date + 60.days ,"$lte": @current_date + 90.days})

    if @nineties_subs.size != 0
      @nineties_subs.each do |sub|
        user = sub.company.user
        days = (sub.end_date - @current_date).to_i
        UserMailer.subs_reminder(user,sub,days).deliver
        sub.ninty_mail_notified = true
        sub.save
        puts sub.inspect
      end
    end
  end

  def get_sixty_days_subs
    @subs = Subscription.sixty_unnotified
    @sixties_subs = @subs.where(end_date: {"$gt": @current_date + 30.days ,"$lte": @current_date + 60.days})

    if @sixties_subs.size != 0
      @sixties_subs.each do |sub|
        user = sub.company.user
        days = (sub.end_date - @current_date).to_i
        UserMailer.subs_reminder(user,sub,days).deliver
        sub.sixty_mail_notified = true
        sub.save
        puts sub.inspect
      end
    end
  end

  def get_thirty_days_subs
    @subs = Subscription.thirty_unnotified
    @thirties_subs = @subs.where(end_date: {"$gt": @current_date, "$lte": @current_date + 30.days})

    if @thirties_subs.size != 0
      @thirties_subs.each do |sub|
        user = sub.company.user
        days = (sub.end_date - @current_date).to_i
        UserMailer.subs_reminder(user,sub,days).deliver
        sub.thirty_mail_notified = true
        sub.save
        puts sub.inspect
      end
    end
  end

end
