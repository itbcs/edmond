class PagesController < ApplicationController
  require "csv"
  include ApplicationHelper
  include ActionView::Helpers::NumberHelper

  def generate_users_csv_infos
    render :generate_users_csv_infos
  end

  def home
    @user = current_user
    if current_user
      sign_out current_user
    end
  end

  def scrapper
    @scrapper = Scrapper.all.last
    @start_page = @scrapper.start_page
    @form = @scrapper.find_form(@start_page[:page])
    @csv = @scrapper.create_csv_file
    (2..4).to_a.each do |number|
      @scrapper.choose_select_option(@form,number-1)
      @region = @scrapper.select_option_value(@form,number-1)

      logger.debug ActiveSupport::LogSubscriber.new.send(:color, "#{@region}", :red, true)

      @result_page = @scrapper.submit_form(@start_page[:agent],@form)
      @item_links = @scrapper.item_links(@result_page)

      if @item_links.size != 0
        @item_links.each do |item_link|
          @item_show_page = item_link.click
          @scrapper.item_values(@item_show_page).each_with_index do |value,index|
            # puts "---- Doc value #{index+1} ----"
            # puts value.content
            # puts "-------------------------"
          end
        end
      end

      @scrapper.pagination_next_link(@result_page,@region)
    end
  end

end
