class Widgets::PagesController < ApplicationController
  protect_from_forgery except: :simulation

  def simulation
    respond_to do |format|
      format.js {render js: js_constructor}
    end
  end

  private

  def js_constructor
    content = render_to_string(params[:template], layout: false)
    puts content.inspect
    "document.write(#{content.to_json})"
  end

end
