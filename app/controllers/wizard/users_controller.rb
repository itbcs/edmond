class Wizard::UsersController < ApplicationController

  def new
    session.delete(:temporary_user_id)
    session.delete(:user_step)
    session.delete(:user_params)
    session[:user_params] ||= {}
    @user = User.new(session[:user_params])
    @user.companies.build
    @user.current_step = session[:user_step]
    puts "--------------"
    puts session[:user_params].inspect
    puts session[:user_step].inspect
    puts "--------------"
  end

  def create
    session[:user_params].deep_merge!(safe_params) if params[:user]
    @user = User.new(session[:user_params])
    @user.current_step = session[:user_step]

    if @user.valid?
      if params[:back_button]
        @user.previous_step
      elsif @user.last_step?
        if @user.all_valid?
          @user.save
        end
      else
        @user.next_step
      end
      session[:user_step] = @user.current_step
    else
      puts "#{@user.errors.full_messages}"
    end

    if @user.new_record?
      if !@user.companies?
        @user.companies.build
      else
        @company = @user.companies.last
        if !@company.shareholders?
          @company.shareholders.build
        end
      end
      render partial: "wizard/partials/#{@user.current_step}_step", locals: {user: @user}
    else
      session.delete(:user_step)
      session.delete(:user_params)
      flash[:notice] = "user saved!"
      sign_in @user
      UserMailer.signup_success(@user).deliver
      @company = @user.companies.last
      ["philippe@bientotchezsoi.com","annegaelle@bientotchezsoi.com","aurelien@bientotchezsoi.com"].each do |email|
        UserMailer.admin_user_signup(email,@user,@company).deliver
      end
      HubspotSendingDatas.call(@user)
      render json: {url: dashboard_start_path}, status: :ok
    end

  end

  def success
    @user = current_user
  end

  private

  def safe_params
    params.require(:user).permit(:is_legal_agent,:password_confirmation,:cgu,:id_card_remote,:personal_email,:phone,:firstname,:email,:lastname,:password,:business_card,:back_button,companies_attributes: [:company_api_search, :legal_name, :trade_name, :company_type, :siren, :balance_closing_date, :localisation, :name, :city, :postal_code, :juridical_nature, :activity_start_date, :full_address, :siret, shareholders_attributes: [:name]])
  end

end
