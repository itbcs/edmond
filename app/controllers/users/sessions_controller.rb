class Users::SessionsController < Devise::SessionsController
  before_action :configure_sign_in_params, only: [:create]
  after_action :after_login, only: [:create]

  def new
    super
  end

  def create
    super
  end

  def destroy
    super
  end

  protected

  def after_login
  end

  def configure_sign_in_params
    devise_parameter_sanitizer.permit(:sign_in, keys: [:firstname, :lastname])
  end

  def after_sign_in_path_for(resource)
    dashboard_start_path
  end

end
