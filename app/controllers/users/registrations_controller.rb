class Users::RegistrationsController < Devise::RegistrationsController
  before_action :configure_sign_up_params

  def new
    super
  end

  def create
    super
  end

  def edit
    super
  end

  def update
    super
  end

  def destroy
    super
  end

  protected

  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:lastname, :firstname])
  end

  def configure_account_update_params
    devise_parameter_sanitizer.permit(:account_update, keys: [:lastname, :firstname])
  end

  def after_sign_up_path_for(resource)
    dashboard_start_path
  end

  def after_update_path_for(resource)
    dashboard_start_path
  end

end
