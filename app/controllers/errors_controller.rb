class ErrorsController < ApplicationController

  def not_found
    puts "---- Inside errors controller / not_found method ----"
    respond_to do |format|
      format.html { render status: 404 }
    end
  end

  def internal_error
    puts "---- Inside errors controller / internal_error method ----"
    respond_to do |format|
      format.html { render status: 500 }
    end
  end

end
