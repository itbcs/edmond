class Dashboard::PollsController < ApplicationController

  def sad
    @user = User.find(params[:id])
    @poll = Poll.find(params[:poll_id])
    @user.polls.push(@poll)
    @poll.negative += 1
    @poll.save
    render partial: "dashboard/polls/partials/seen", locals: {poll: @poll}
  end

  def mitigate
    @user = User.find(params[:id])
    @poll = Poll.find(params[:poll_id])
    @user.polls.push(@poll)
    @poll.mitigate += 1
    @poll.save
    render partial: "dashboard/polls/partials/seen", locals: {poll: @poll}
  end

  def happy
    @user = User.find(params[:id])
    @poll = Poll.find(params[:poll_id])
    @user.polls.push(@poll)
    @poll.positive += 1
    @poll.save
    render partial: "dashboard/polls/partials/seen", locals: {poll: @poll}
  end

end
