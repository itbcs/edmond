class Dashboard::CompaniesController < ApplicationController

  def show
  end

  def index
    @companies = current_user.companies
    @company = current_user.companies.last
    @current_page = dashboard_companies_path

    if request.xhr?
      render :index, layout: false
    else
      render :index, layout: "ajax"
    end
  end

  def edit
    puts "---- PARAMS ----"
    puts params.inspect
    puts "----------------"
    @company = Company.find(params[:id])
    render :edit, layout: false
  end

  def update
    puts "---- PARAMS COMPANY UPDATE ----"
    puts params.inspect
    puts "----------------"
    @company = Company.find(params[:id])
    if @company.update(safe_params)
      @current_page = dashboard_companies_path
      render :index, layout: false
    else
      render :edit, layout: false
    end
  end

  def new
  end

  def destroy
  end

  private

  def safe_params
    params.require(:company).permit(:company_status, :kbis, :official_attestation, :name, :legal_name, :trade_name, :siret, :siren, :company_type, :localisation)
  end

end
