class Dashboard::PagesController < ApplicationController
  before_action :authenticate_user!

  def start
    @companies = current_user.companies
  end

  def synthesis
    # flash.now[:info] = {title: "Info", template: "info", content: "Cette page représente une synthèse de votre activité", date: l(Time.now, format: :time_without_seconds)}
    # flash.now[:warning] = {title: "Attention", template: "warning",content: "Vous n'avez pas de société renseignée", date: l(Time.now, format: :time_without_seconds)}
    # flash.now[:reminder] = {title: "Rappel", template: "reminder",content: "Certains de vos compromis arrivent à expiration aujourd'hui", date: l(Time.now, format: :time_without_seconds)}
    @user = current_user
    @company = current_user.companies.includes(:compromises, :costs, :subscriptions).last
    @compromises = @company.compromises

    @management_datas = GenerateManagementDatas.call(@company,:halfyear).result
    logger.debug ActiveSupport::LogSubscriber.new.send(:color, "#{@management_datas}", :red, true)

    @poll = Poll.all.sample
    @filling_poll = current_user.polls.find(@poll.id.to_s)
    @last_signed_compromise = @compromises.where(status: 0).sort_by{|item| item.authentic_act_signature_date}.last


    # @notifs_list = []
    # @notifications = Subscription.company_subs_end({company_id: @company.id.to_s, start: 30, end: 60 })
    # if @notifications.size != 0
    #   @notifications.each do |notif|
    #     logger.debug ActiveSupport::LogSubscriber.new.send(:color, "#{notif.inspect}", :red, true)
    #     days = (notif.end_date - Date.today).to_i
    #     @notifs_list << {title: "Info", template: "info", content: "Votre abonnement <b>#{notif.name}</b> se termine dans #{days} jours".html_safe, date: l(Time.now, format: :time_without_seconds)}
    #   end
    # end
    # flash.now[:info] = @notifs_list

    if @user.has_platform_access?
      if request.xhr?
        render :synthesis, layout: false
      else
        render :synthesis, layout: "ajax"
      end
    else
      @restricted_access = true
      if request.xhr?
        render :synthesis, layout: false
      else
        render :synthesis, layout: "ajax"
      end
    end

  end

  def restricted_synthesis
    if request.xhr?
      render :restricted_synthesis, layout: false
    else
      render :restricted_synthesis, layout: "ajax"
    end
  end


  def management
    @user = current_user
    @company = current_user.companies.includes(:compromises, :costs, :subscriptions).last
    @management_datas = GenerateManagementDatas.call(@company,:fullyear).result
    logger.debug ActiveSupport::LogSubscriber.new.send(:color, "#{@management_datas}", :yellow, true)

    if @user.has_platform_access?
      if request.xhr?
        render :management, layout: false
      else
        render :management, layout: "ajax"
      end
    else
      @restricted_access = true
      if request.xhr?
        render :management, layout: false
      else
        render :management, layout: "ajax"
      end
    end

  end

  def sells
    @company = current_user.companies.last
    @compromises = @company.compromises
    if request.xhr?
      puts "Xhr request"
      render partial: "dashboard/pages/partials/management/sells"
    else
      puts "Classic request"
      render "dashboard/pages/ajax/sells", layout: "ajax_with_tabs"
    end
  end

  def costs
    @year = Date.today.year
    @month = Date.today.month
    @company = current_user.companies.last
    @costs = current_user.companies.last.costs
    @cost = @costs.find_by(month: Date.today.month.to_i,year: Date.today.year.to_i)

    @date = Date.parse("#{@year}/#{@month}")
    current_month_subs_sum = @company.subscriptions.where(:start_date.lte => @date, :end_date.gt => @date).map{|sub| sub.int_amount}.sum

    if @cost
      @datas = [@cost.rent.to_i,@cost.wages.to_i,@cost.subscriptions.to_i,@cost.others.to_i]
      @datas[2] = current_month_subs_sum ? current_month_subs_sum : 0
      @costs_total = @datas.sum
    else
      @datas = [0,0,current_month_subs_sum,0]
      @new_cost = Cost.new
    end
    render partial: "dashboard/pages/partials/management/costs", locals: {datas: @datas}
  end

  def treasury
    @company = current_user.companies.last
    @compromises = @company.compromises
    @costs = current_user.companies.last.costs

    @management_datas = GenerateManagementDatas.call(@company,:fullyear).result
    logger.debug ActiveSupport::LogSubscriber.new.send(:color, "#{@management_datas}", :yellow, true)

    if request.xhr?
      render partial: "dashboard/pages/partials/management/treasury"
    else
      render "dashboard/pages/ajax/treasury", layout: "ajax_with_tabs"
    end
  end

end
