class Dashboard::UserQuestionsController < ApplicationController

  def new
    @user_question = UserQuestion.new
    render partial: "dashboard/user_questions/partials/new", locals: {user_question: @user_question}
  end

  def create
    @user_question = UserQuestion.new(safe_params)
    if @user_question.save
      @user = User.find(@user_question.user_id)
      @company = @user.companies.last

      ["philippe@bientotchezsoi.com","annegaelle@bientotchezsoi.com","aurelien@bientotchezsoi.com"].each do |email|
        UserMailer.user_question(@user,@user_question,@company,email).deliver
      end

      render "dashboard/user_questions/success", layout: false
    else
      render partial: "dashboard/user_questions/partials/new", locals: {user_question: @user_question}
    end
  end

  private

  def safe_params
    params.require(:user_question).permit(:user_id, :object, :message)
  end

end
