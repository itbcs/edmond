class Dashboard::FinancingRequestsController < ApplicationController

  def new
    @user = current_user
    @company = @user.companies.last
    @financing_request = FinancingRequest.new
    if request.xhr?
      render :new, layout: false
    else
      render :new, layout: "ajax"
    end
  end

  def create
    @financing_request = FinancingRequest.new(safe_params)
    if @financing_request.save
      puts "---- FinancingRequest object ----"
      puts @financing_request.inspect
      @user = @financing_request.company.user

      ["philippe@bientotchezsoi.com","annegaelle@bientotchezsoi.com","aurelien@bientotchezsoi.com"].each do |email|
        UserMailer.admin_financing_request(@user,@user.companies.last,@financing_request,email).deliver
      end

      UserMailer.financing_request(@user).deliver
      render :success, layout: false
    else
      puts @financing_request.errors.full_messages
      @user = current_user
      @company = @user.companies.last
      render :new, layout: false
    end
  end

  def success
    render :success, layout: false
  end

  private

  def safe_params
    params.require(:financing_request).permit(:company_id,:year_transactions_count,:pending_transactions_count,:annual_revenue,:total_transactions_amount,:company_creation_year)
  end

end
