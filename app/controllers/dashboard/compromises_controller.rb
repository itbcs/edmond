class Dashboard::CompromisesController < ApplicationController
  before_action :authenticate_user!, except: [:validation]
  skip_before_action :verify_authenticity_token, only: [:signed,:change_signed_date]

  def show
    @compromise = Compromise.find(params[:id])
    @referer = dashboard_compromises_path
    @user_question = UserQuestion.new
    @referer_title = "Retour au tableau des compromis"
    if request.xhr?
      render :show, layout: false
    else
      render :show, layout: "ajax"
    end
  end

  def index
    @companies = current_user.companies
    @company = current_user.companies.last
    @compromises = @companies.to_a.map{|company| company.compromises}.flatten

    @resource_name = "#{controller_name}-#{action_name}"
    # flash.now[:info] = {title: "Info", content: "Retrouvez ici tous vos compromis", template: "info", date: l(Time.now, format: :time_without_seconds)}

    if current_user.has_platform_access?
      if request.xhr?
        render :index, layout: false
      else
        render :index, layout: "ajax"
      end
    else
      @restricted_access = true
      if request.xhr?
        render :index, layout: false
      else
        render :index, layout: "ajax"
      end
    end
  end

  def new
    @company = current_user.companies.last
    @compromise = @company.compromises.build
    if request.xhr?
      render :new, layout: false
    else
      render :new, layout: "ajax"
    end
  end

  def edit
    @compromise = Compromise.find(params[:id])
    render :edit, layout: false
  end

  def update
    @compromise = Compromise.find(params[:id])
    if @compromise.update(safe_params)
      @user_question = UserQuestion.new
      render :show, layout: false
    else
      puts @compromise.errors_full_messages
      render :edit, layout: false
    end
  end

  def new_pending
    @company = current_user.companies.last
    @compromise = @company.compromises.build
    render :new_pending, layout: false
  end

  def validation
    puts params
    puts params[:id]
    @compromise = Compromise.find(params[:id])
    @user = @compromise.company.user

    if @user
      sign_in @user
    end

    puts @compromise
    render :validation
  end

  def signed
    @compromise = Compromise.find(params[:id])
    @compromise[:status] = 0
    if @compromise.save
      render :validation_success, layout: false
    else
      puts @compromise.errors.full_messages
      render :validation_fail, layout: false
    end
  end

  def change_signed_date
    @compromise = Compromise.find(params[:id])
    render :change_signed_date, layout: false
  end

  def update_act_signature_date
    @compromise = Compromise.find(params[:id])
    if @compromise.update(safe_params)
      render :validation_success, layout: false
    else
      render :change_signed_date, layout: false
    end
  end

  def create
    @compromise = Compromise.new(safe_params)
    if @compromise.save
      @company = current_user.companies.last
      @compromises = current_user.companies.last.compromises
      @resource_name = "compromises-index"

      current_user.points += 150
      current_user.save
      # flash.now[:create] = {title: "Compromis crée", instance: @compromise, template: "create", date: l(Time.now, format: :time_without_seconds)}
      render :index, layout: false
    else
      puts  @compromise.errors.full_messages
      if params[:compromise][:pending_compromise]
        render :new, layout: false
      else
        render :new_pending, layout: false
      end
    end
  end

  private

  def safe_params
    params.require(:compromise).permit(:pending_compromise,:authentic_act_signature_date,:compromise_signature_place,:selling_price,:pending_funding_terms,:contract_type,:compromise_signature_date,:seller_lastname,:status,:reference,:fees,:purchase_type,:company_id)
  end

end
