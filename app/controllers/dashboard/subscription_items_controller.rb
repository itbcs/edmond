class Dashboard::SubscriptionItemsController < ApplicationController

  def show
    @company = current_user.companies.last
    @subscription = Subscription.new
    @sub_item = SubscriptionItem.find(params[:id])
    render :show, layout: false
  end

  def edit
    @company = current_user.companies.last
    @sub_item = SubscriptionItem.find(params[:id])
    @sub_type = @sub_item.subscription_type

    puts "---- Subs by category ----"
    puts @company.subscriptions_by_category
    puts "--------------------------"

    @result = SubscriptionItem.slug_names & @company.subscriptions_names

    @subscription = @company.subscriptions.find_by(slug_name: @sub_item.slug_name)
    render :edit, layout: false
  end

end
