class Dashboard::SubscriptionsController < ApplicationController
  protect_from_forgery unless: -> { request.xhr? }

  def new
    @company = current_user.companies.last
    @subscription = Subscription.new

    @first_subscription_type = SubscriptionType.find_by(input_value: 1)
    @subs_items = @first_subscription_type.subscription_items
    @sorted_subs_category = @company.subscriptions_by_category

    render :new, layout: false
  end

  def show
    @company = current_user.companies.last
    @management_datas = GenerateManagementDatas.call(@company,:fullyear).result

    @subscription = Subscription.find(params[:id])
    render :show, layout: false
  end

  def create
    @sub_item = SubscriptionItem.find(params[:subscription][:subitem_id])
    @company = current_user.companies.last
    @subscription = Subscription.new(safe_params)
    if @subscription.save
      @subscriptions = @company.subscriptions
      @resource_name = "#{controller_name}-#{action_name}"
      render "dashboard/subscription_items/edit", layout: false
    else
      puts @subscription.errors.full_messages
      render "dashboard/subscription_items/show", layout: false
    end
  end

  def edit
    @company = current_user.companies.last
    @subscription = Subscription.find(params[:id])
    render :edit, layout: false
  end

  def update
    @company = current_user.companies.last
    @subscription = Subscription.find(params[:id])
    @sub_item = SubscriptionItem.find(@subscription.subitem_id)
    logger.debug ActiveSupport::LogSubscriber.new.send(:color, "#{safe_params}", :yellow)

    if @subscription.update(safe_params)
      @subscriptions = @company.subscriptions
      @management_datas = GenerateManagementDatas.call(@company,:fullyear).result
      render :show, layout: false
    else
      puts @subscription.errors.full_messages
      render "dashboard/subscription_items/show", layout: false
    end
  end

  def index
    @company = current_user.companies.last
    @resource_name = "#{controller_name}-#{action_name}"
    if request.xhr?
      render :index, layout: false
    else
      render :index, layout: "ajax"
    end
  end

  def dynamic_field
    @field = params[:field]
    @resource = params[:resource]
    render :dynamic_field, layout: false
  end

  def close
    @subscription = Subscription.find(params[:id])
    render :close, layout: false
  end

  def renew
    @subscription = Subscription.find(params[:id])
    render :renew, layout: false
  end

  def destroy
    @company = current_user.companies.last
    @subscription = Subscription.find(params[:id])
    if @subscription.destroy
      @management_datas = GenerateManagementDatas.call(@company,:fullyear).result
      render "dashboard/pages/management", layout: false
    else
      @management_datas = GenerateManagementDatas.call(@company,:fullyear).result
      render :show, layout: false
    end
  end

  private

  def safe_params
    params.require(:subscription).permit(:name,:category,:amount,:start_date,:end_date,:company_id,:subitem_id,:slug_name,:recurrence,:closed,:renewed)
  end

end
