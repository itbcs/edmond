class Dashboard::CostsController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:month_costs, :year_costs]

  def new
    @company = current_user.companies.last
    if @company.cost_reference
      puts @company.cost_reference
    else
      puts "There is no cost reference for this company !!!!"
    end
    @cost = Cost.new
    render :new, layout: false
  end

  def create
    @cost = Cost.new(safe_params)
    @company = current_user.companies.last
    @month = params[:cost][:month].to_i
    @year = params[:cost][:year].to_i

    @date = Date.parse("#{@year}/#{@month}")
    current_month_subs_sum = @company.subscriptions.where(:start_date.lte => @date, :end_date.gt => @date).map{|sub| sub.int_amount}.sum

    if @cost.save
      @costs = @company.costs
      @datas = [@cost.rent.to_i,@cost.wages.to_i,@cost.subscriptions.to_i,@cost.others.to_i]
      @datas[2] = current_month_subs_sum ? current_month_subs_sum : 0
      @costs_total = @datas.sum
      render partial: "dashboard/costs/partials/show", locals: {cost: @cost, month: @month, datas: @datas, costs_total: @costs_total}
    else
      puts @cost.errors.full_messages
      render :new, layout: false
    end
  end

  def edit
    @cost = Cost.find(params[:id])
    @company = @cost.company
    @month = @cost.month.to_i
    @year = @cost.year.to_i

    @datas = [@cost.rent.to_i,@cost.wages.to_i,@cost.subscriptions.to_i,@cost.others.to_i]

    @date = Date.parse("#{@year}/#{@month}")
    current_month_subs_sum = @company.subscriptions.where(:start_date.lte => @date, :end_date.gt => @date).map{|sub| sub.int_amount}.sum

    @datas[2] = current_month_subs_sum ? current_month_subs_sum : 0
    @costs_total = @datas.sum

    render :edit, layout: false
  end

  def update
    @cost = Cost.find(params[:id])
    @company = @cost.company
    if @cost.update(safe_params)
      render partial: "dashboard/costs/partials/show", locals: {cost: @cost, month: @cost.month, datas: @cost.datas, costs_total: @cost.total, company: @company}
    else
      render :edit, layout: false
    end
  end

  def show
    @cost = Cost.find(params[:id])
    render :show
  end

  def month_costs
    @company = current_user.companies.last
    @month = params["month"].to_i
    @year = params["year"].to_i
    @cost = current_user.companies.last.costs.find_by(month: @month, year: @year)

    @date = Date.parse("#{@year}/#{@month}")
    current_month_subs_sum = @company.subscriptions.where(:start_date.lte => @date, :end_date.gt => @date).map{|sub| sub.int_amount}.sum

    if @cost
      @datas = [@cost.rent.to_i,@cost.wages.to_i,@cost.subscriptions.to_i,@cost.others.to_i]
      @month_subs_sum = @company.subscriptions_amount_sum
      @datas[2] = current_month_subs_sum ? current_month_subs_sum : 0
      @costs_total = @datas.sum
      render partial: "dashboard/costs/partials/show", locals: {cost: @cost, month: @month, datas: @datas, costs_total: @costs_total}
    else
      @datas = [0,0,current_month_subs_sum,0]
      @cost = Cost.new
      render :new, layout: false
    end
  end

  def year_costs
    @year = params[:year]
    @month = 1
    @company = current_user.companies.last
    @cost = @company.costs.find_by(month: 1,year: @year)

    if @company.cost_reference
      puts @company.cost_reference
    else
      puts "There is no cost reference for this company !!!!"
    end

    if @cost
      @costs = current_user.companies.last.costs
      @datas = [@cost.rent.to_i,@cost.wages.to_i,@cost.subscriptions.to_i,@cost.others.to_i]
      @sub_total = @company.subscriptions_amount_sum
      @datas[2] = @sub_total ? @sub_total : 0
      @costs_total = @datas.sum
    else
      @company = current_user.companies.last
      @month = 1
      @new_cost = Cost.new
    end
    render partial: "dashboard/pages/partials/management/costs"
  end

  private

  def safe_params
    params.require(:cost).permit(:month, :year, :rent, :subscriptions, :wages, :others, :company_id)
  end

end
