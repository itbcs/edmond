class Dashboard::SubscriptionTypesController < ApplicationController

  def show
    @company = current_user.companies.last
    @sub = SubscriptionType.find(params[:id])
    @subs_items = @sub.subscription_items
    render :show, layout: false
  end

end
