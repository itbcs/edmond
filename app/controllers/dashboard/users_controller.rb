class Dashboard::UsersController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:stripe_subscription_cancel]

  def edit
    @user = current_user
    render :edit, layout: false
  end

  def update
    @user = current_user
    @company = @user.companies.last

    puts "---- Password ----"
    if params[:user][:password].size != 0
      if @user.update(safe_params)
        bypass_sign_in(@user)
        render "dashboard/companies/index", layout: false
      else
        puts @user.errors.full_messages
        render :edit, layout: false
      end
    else
      if @user.update_without_password(safe_params)
        bypass_sign_in(@user)
        render "dashboard/companies/index", layout: false
      else
        puts @user.errors.full_messages
        render :edit, layout: false
      end
    end
  end

  def infos
    @user = current_user
    if @user.has_platform_access?
      @datas = nil
    else
      @datas = InitiateStripeAccount.call(current_user, dashboard_subscription_success_url, root_url).result
    end
    if request.xhr?
      render :infos, layout: false
    else
      render :infos, layout: "ajax"
    end
  end

  def stripe_subscription_cancel
    @user = current_user
    if current_user.stripe_subscription_id
      @deleted_sub = Stripe::Subscription.delete(current_user.stripe_subscription_id)
      new_status = @deleted_sub.as_json["status"]
      @user.stripe_subscription_status = new_status
      if @user.save
        @datas = InitiateStripeAccount.call(current_user, dashboard_subscription_success_url, root_url).result
        puts "User save with new stripe subscription status"
      else
        puts "User save failed"
      end
    end
    if request.xhr?
      render :infos, layout: false
    else
      render :infos, layout: "ajax"
    end
  end

  def subscription_success
    if request.xhr?
      render :subscription_success, layout: false
    else
      render :subscription_success, layout: "ajax"
    end
  end

  def coupon_code
    values = {}
    Stripe::Coupon.list.as_json["data"].map{|coupon| values["#{coupon['name']}"] = coupon["id"] }
    logger.debug ActiveSupport::LogSubscriber.new.send(:color, "#{values}", :yellow, true)
    if params["coupon_code"]["code"] != ""
      if values.has_key?(params["coupon_code"]["code"])
        coupon_id = values[params["coupon_code"]["code"]].to_s
        updated_sub = Stripe::Subscription.update(current_user.stripe_subscription_id, {coupon: coupon_id})
        puts updated_sub.inspect
        content = render_to_string("dashboard/users/valid_coupon", layout: false)
        render json: {redirect: dashboard_subscription_infos_path, html: content, status: 200}
        # render "dashboard/users/valid_coupon", layout: false
      else
        puts "Invalid promo code"
        @error = "Code invalide - #{params['coupon_code']['code']}"
        render "dashboard/users/coupon", layout: false
      end
    else
      puts "Empty input value"
      @error = "Code manquant"
      render "dashboard/users/coupon", layout: false
    end
  end

  private

  def safe_params
    params.require(:user).permit(:firstname,:lastname,:email,:password,:password_confirmation,:phone)
  end

end
