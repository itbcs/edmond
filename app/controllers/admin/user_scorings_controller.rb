class Admin::UserScoringsController < ApplicationController
  layout false

  def show
  end

  def index
  end

  def new
  end

  def create
    @user_scoring = UserScoring.new(safe_params)
  end

  def edit
    @user_scoring = UserScoring.find(params[:id])
  end

  def update
    @user_scoring = UserScoring.find(params[:id])
    @company = @user_scoring.user.companies.last
    if @user_scoring.update(safe_params)
      @activity = UserActivity.call(@company).result
      @scoring = UserScoringCalculation.call(@user_scoring.user).result
      redirect_to admin_company_path(@company)
    else
      render :show, layout: "admin"
    end
  end

  private

  def safe_params
    params.require(:user_scoring).permit(:funds, :incident, :min_balance_sheets, :n_minus_one_result, :n_minus_two_result, :treasury, :treasury_incident, :activity_turnover_evolution, :activity_transaction_evolution, :already_customer, :have_website, :sign, :active_on_social_networks, :one_year_market_trend, :market_confidence_index, :compromises_failure_rate)
  end

end
