class Admin::PagesController < ApplicationController
  before_action :authenticate_admin!

  def start
    @requests = FinancingRequest.all
    render :start, layout: "admin"
  end

end
