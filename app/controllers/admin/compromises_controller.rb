class Admin::CompromisesController < ApplicationController

  def show
    @compromise = Compromise.find(params[:id])
    @compromise_scoring = CompromiseScoringCalculation.call(@compromise).result

    if @compromise.contract_type == nil
      flash[:missing_field] = "Type de contrat manquant"
    end

    render :show, layout: "admin"
  end

end
