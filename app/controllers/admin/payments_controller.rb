class Admin::PaymentsController < ApplicationController
  before_action :authenticate_user!
  skip_before_action :verify_authenticity_token, only: [:payment_data]

  def test_api_call
    # logger.debug ActiveSupport::LogSubscriber.new.send(:color, "#{root_url}", :blue, true)
    # @success_url = (admin_payment_success_url + "?session_id={CHECKOUT_SESSION_ID}")
    # if current_user.stripe_customer_id

    #   customer = Stripe::Customer.retrieve(current_user.stripe_customer_id)
    #   ruby_hash = customer["invoice_settings"].to_h

    #   if ruby_hash[:default_payment_method] == nil
    #   end

    #   customer_default_payment_method = customer["subscriptions"]["data"][0] ? customer["subscriptions"]["data"][0]["default_payment_method"] : nil

    #   subscription_id = customer["subscriptions"]["data"][0]["id"]
    #   puts subscription_id

    #   if customer_default_payment_method
    #     Stripe::Customer.update(current_user.stripe_customer_id, {invoice_settings: {default_payment_method: customer_default_payment_method}})
    #   end

    #   customer = Stripe::Customer.retrieve(current_user.stripe_customer_id)
    #   logger.debug ActiveSupport::LogSubscriber.new.send(:color, "#{customer}", :yellow, true)

    #   options = {locale: "fr", client_reference_id: current_user.id.to_s, customer: current_user.stripe_customer_id, payment_method_types: ['card'], subscription_data: {items: [{plan: 'plan_GjUnomo01V2dA4'}]}, success_url: @success_url, cancel_url: 'https://example.com/cancel'}
    #   annual_options = {locale: "fr", client_reference_id: current_user.id.to_s, customer_email: current_user.email, payment_method_types: ['card'], subscription_data: {items: [{plan: 'plan_GjUrz6QVm55DGs'}]}, success_url: @success_url, cancel_url: 'https://example.com/cancel'}
    #   # options = {locale: "fr", mode: "setup", setup_intent_data: {metadata: {customer_id: current_user.stripe_customer_id, subscription_id: subscription_id},}, payment_method_types: ['card'], success_url: @success_url, cancel_url: 'https://example.com/cancel'}
    # else
    #   options = {locale: "fr", client_reference_id: current_user.id.to_s, customer_email: current_user.email, payment_method_types: ['card'], subscription_data: {items: [{plan: 'plan_GjUnomo01V2dA4'}]}, success_url: @success_url, cancel_url: 'https://example.com/cancel'}
    #   annual_options = {locale: "fr", client_reference_id: current_user.id.to_s, customer_email: current_user.email, payment_method_types: ['card'], subscription_data: {items: [{plan: 'plan_GjUrz6QVm55DGs'}]}, success_url: @success_url, cancel_url: 'https://example.com/cancel'}
    # end
    # session = Stripe::Checkout::Session.create(options)
    # @monthly_session_id = session["id"]

    # @annual_session_id = Stripe::Checkout::Session.create(annual_options)["id"]

    # if current_user.stripe_customer_id
    #   user_subs = Stripe::Subscription.list({customer: current_user.stripe_customer_id})
    #   active_sub = user_subs["data"].first["plan"]["active"]
    #   active_sub_boolean = ActiveModel::Type::Boolean.new.cast(active_sub)
    #   puts active_sub_boolean.inspect

    #   payments = Stripe::PaymentMethod.list({
    #     customer: current_user.stripe_customer_id,
    #     type: 'card',
    #   })

    #   card = payments["data"].first
    #   brand = card["card"]["brand"]
    #   country = card["card"]["country"]
    #   exp_month = card["card"]["exp_month"]
    #   exp_year = card["card"]["exp_year"]
    #   last4 = card["card"]["last4"]

    #   @card_infos = {brand: brand, country: country, exp_month: exp_month, exp_year: exp_year, last4: last4}
    #   logger.debug ActiveSupport::LogSubscriber.new.send(:color, "#{@card_infos}", :red, true)

    # end
    @datas = InitiateStripeAccount.call(current_user, dashboard_subscription_success_url, root_url).result
    render :api_call
  end

  def success
    session = Stripe::Checkout::Session.retrieve(params["session_id"])
    puts session.inspect
    # stripe_customer_id = session["customer"]
    # current_user.stripe_customer_id = stripe_customer_id
    # current_user.save(validate: false)
    render :success
  end

  def payment_data
    payment_id = params["payment"]
    logger.debug ActiveSupport::LogSubscriber.new.send(:color, "#{payment_id}", :red, true)

    customer = Stripe::Customer.create({
      payment_method: payment_id,
      invoice_settings: {
        default_payment_method: payment_id
      },
      email: current_user.email
    })

    logger.debug ActiveSupport::LogSubscriber.new.send(:color, "#{customer["id"]}", :yellow, true)

    if customer["id"]
      current_user.stripe_customer_id = customer["id"]
      current_user.save(validate: false)
    end

    Stripe::Subscription.create({
      customer: customer["id"],
      items: [{plan: 'plan_GjUnomo01V2dA4'}],
      off_session: true
    })

    render json: {text: "Successfull payment data"}, status: 200
  end

end
