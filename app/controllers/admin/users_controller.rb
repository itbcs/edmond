class Admin::UsersController < ApplicationController
  before_action :authenticate_admin!
  # http_basic_authenticate_with name: Rails.application.credentials.http_auth_username, password: Rails.application.credentials.http_auth_password

  def index
    @users = User.includes(:companies).sort_by{|user| user.created_at}.reverse
    render :index, layout: "admin"
  end

  def show
    @user = User.includes(:companies).find(params[:id])
    @company = @user.companies.last
    render :show, layout: "admin"
  end

end
