class Admin::Webhooks::Payments::SubscriptionsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def trial_will_end
    event = JSON.parse(request.body.read)
    logger.debug ActiveSupport::LogSubscriber.new.send(:color, "#{event}", :red, true)
    customer_id = event["data"]["object"]["customer"]
    @user = User.find_by(stripe_customer_id: customer_id)
    if @user
      render json: {user: @user.as_json, status: 200}
    else
      render json: {rails_status: "Initiate webhook for customer.subscription.trial_will_end event", status: 200}
    end
  end

  def updated
    event = JSON.parse(request.body.read)
    logger.debug ActiveSupport::LogSubscriber.new.send(:color, "#{event}", :yellow, true)
    @user = User.find_by(stripe_customer_id: event.as_json["data"]["object"]["customer"])

    coupon = event.as_json["data"]["object"]["discount"]["coupon"]
    if coupon
      coupon_id = coupon["id"]
      coupon_name = coupon["name"]
      coupon_percent_off = coupon["percent_off"]
      coupon_amount_off = coupon["amount_off"]
      @user.stripe_coupon_id = coupon_id
      @user.stripe_coupon_name = coupon_name

      if coupon_percent_off
        @user.stripe_coupon_percent_off = coupon_percent_off
      elsif coupon_amount_off
        @user.stripe_coupon_amount_off = (coupon_amount_off.to_f / 100.0).to_f
      end

      if @user.save(validate: false)
        puts "User successfully saved with coupon infos"
      else
        puts "An error occured when try to update user"
      end
    end

    render json: {user: @user.as_json, status: 200}
  end

  def deleted
    event = JSON.parse(request.body.read)
    logger.debug ActiveSupport::LogSubscriber.new.send(:color, "#{event}", :red, true)
    render json: {rails_status: "Initiate webhook endpoint for customer.subscription.deleted event", status: 200}
  end

end
