class Admin::Webhooks::Payments::CustomersController < ApplicationController
  skip_before_action :verify_authenticity_token

  def updated
    event = JSON.parse(request.body.read)
    logger.debug ActiveSupport::LogSubscriber.new.send(:color, "#{event}", :yellow, true)
    render json: {rails_status: "Initiate webhook endpoint for customer.updated event", status: 200}
  end

end
