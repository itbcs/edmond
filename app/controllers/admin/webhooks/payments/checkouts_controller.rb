class Admin::Webhooks::Payments::CheckoutsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def completed
    event = JSON.parse(request.body.read)
    logger.debug ActiveSupport::LogSubscriber.new.send(:color, "#{event}", :red, true)
    rails_user_id = event["data"]["object"]["client_reference_id"]
    stripe_customer_id = event["data"]["object"]["customer"]
    @user = User.find(rails_user_id)
    @user.stripe_customer_id = stripe_customer_id
    @company = @user.companies.last
    @user.save(validate: false)


    customer = @user.retrieve_stripe_account
    if customer
      customer_default_payment_method = customer["subscriptions"]["data"][0] ? customer["subscriptions"]["data"][0]["default_payment_method"] : nil
      subscription_id = customer["subscriptions"]["data"][0]["id"]
      if subscription_id
        @user.stripe_subscription_id = subscription_id
      end

      if customer_default_payment_method
        Stripe::Customer.update(@user.stripe_customer_id, {invoice_settings: {default_payment_method: customer_default_payment_method}, phone: @user.phone, description: @company.name, address: {line1: @company.full_address, city: @company.city, country: "FR", postal_code: @company.postal_code}})
      end
      @user.payment_method_id = customer_default_payment_method
    end

    user_subs = Stripe::Subscription.list({customer: @user.stripe_customer_id})
    active_sub = user_subs["data"].first["plan"]["active"]
    active_sub_boolean = ActiveModel::Type::Boolean.new.cast(active_sub)
    @user.active_stripe_subscription = active_sub_boolean


    if @user.stripe_subscription_id
      user_subscription = Stripe::Subscription.retrieve(@user.stripe_subscription_id)
      interval = user_subscription["plan"]["interval"]
      plan_amount = '%.2f' % (user_subscription["plan"]["amount"].to_f / 100)
      trial_period = user_subscription["plan"]["trial_period_days"].to_i

      if interval == "month"
        french_interval = "mois"
      elsif interval == "year"
        french_interval = "an"
      end

      @user.stripe_subscription_plan_amount = "#{plan_amount} € / #{french_interval}"
      @user.stripe_plan_interval = french_interval
      @user.stripe_plan_amount = plan_amount
      @user.stripe_trial_period_days = trial_period
    end

    if @user.stripe_subscription
      product_id = @user.stripe_subscription.as_json["items"]["data"][0]["plan"]["product"]
      start_date = Time.at(@user.stripe_subscription.as_json["start_date"]).to_datetime
      status = @user.stripe_subscription.as_json["status"]

      @user.stripe_subscription_status = status
      @user.stripe_product_id = product_id
      @user.stripe_subscription_start_date = start_date
    end

    if @user.payment_method_id
      payment_method = Stripe::PaymentMethod.retrieve(@user.payment_method_id,)
      card_infos = {brand: payment_method["card"]["brand"], country: payment_method["card"]["country"], exp_month: payment_method["card"]["exp_month"], exp_year: payment_method["card"]["exp_year"], last4: payment_method["card"]["last4"]}
      @user.stripe_card_datas = card_infos.to_h
    end

    if @user.stripe_product
      product_name = @user.stripe_product.as_json["name"]
      @user.stripe_product_name = product_name
    end

    @user.save(validate: false)
    render json: {object: @user.as_json, status: 200}
  end

end
