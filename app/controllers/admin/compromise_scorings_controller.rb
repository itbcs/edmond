class Admin::CompromiseScoringsController < ApplicationController

  def edit
  end

  def update
    @compromise_scoring = CompromiseScoring.find(params[:id])
    if @compromise_scoring.update(safe_params)
      flash[:missing_field] = "Scoring mis à jour avec succès"
      redirect_to admin_compromise_path(@compromise_scoring.compromise)
    else
      render "admin/compromises/show", layout: "admin"
    end
  end

  private

  def safe_params
    params.require(:compromise_scoring).permit(:sale_type, :seller_type, :writing_quality, :weak_signals, :seller_risk, :buyer_risk)
  end

end
