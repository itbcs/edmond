class Admin::AdminsController < ApplicationController
  before_action :authenticate_admin!

  def show
    @admin = Admin.find(params[:id])
    render :show, layout: "admin"
  end

  def index
    @admins = Admin.all
    @compromises = Compromise.all
    render :index, layout: "admin"
  end

end
