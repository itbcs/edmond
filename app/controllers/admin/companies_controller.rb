class Admin::CompaniesController < ApplicationController
  before_action :authenticate_admin!

  def index
    @companies = Company.all

    @companies.each do |company|
      if company.user == nil
        company.compromises? ? company.compromises.destroy_all : nil
        company.costs? ? company.costs.destroy_all : nil
        company.subscriptions? ? company.subscriptions.destroy_all : nil
        company.financing_requests? ? company.financing_requests.destroy_all : nil
        company.destroy
      end
    end

    render :index, layout: "admin"
  end

  def show
    @companies = Company.all
    @company = Company.find(params[:id])

    if @company.activity_start_date == nil
      flash[:missing_field] = "Date de début d'activité manquante"
    end

    if @company.user == nil
      @company.compromises? ? @company.compromises.destroy_all : nil
      @company.costs? ? @company.costs.destroy_all : nil
      @company.subscriptions? ? @company.subscriptions.destroy_all : nil
      @company.financing_requests? ? @company.financing_requests.destroy_all : nil
      render :company_without_user, layout: "admin"
    else
      @activity = UserActivity.call(@company).result
      @scoring = UserScoringCalculation.call(@company.user).result
      logger.debug ActiveSupport::LogSubscriber.new.send(:color, "#{@activity}", :red, true)
      logger.debug ActiveSupport::LogSubscriber.new.send(:color, "#{@scoring}", :yellow, true)
      render :show, layout: "admin"
    end
  end

end
