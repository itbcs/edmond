class Admin::FinancingRequestsController < ApplicationController
  before_action :authenticate_admin!

  def show
    @financing_request = FinancingRequest.find(params[:id])
    render :show, layout: "admin"
  end

  def index
    @financing_requests = FinancingRequest.all
    render :index, layout: "admin"
  end

  def update
    @financing_request = FinancingRequest.find(params[:id])
    if @financing_request.update(safe_params)
      redirect_to admin_financing_request_path(@financing_request), layout: "admin"
    else
      logger.debug ActiveSupport::LogSubscriber.new.send(:color, "#{@financing_request.errors.full_messages}", :red, true)
      render :show, layout: "admin"
    end
  end

  private

  def safe_params
    params.require(:financing_request).permit(:status)
  end

end
