class ApplicationController < ActionController::Base
  after_action { flash.clear if request.xhr? }
  before_action :set_request_path
  after_action :display_request

  def not_found_redirect
    puts "---- Inside not found redirect method ----"
    redirect_to notfound_path
  end

  private

  def display_request
    puts "---- DISPLAY REQUEST ----"
    puts request.referer
    puts "-------------------------"
  end

  def set_request_path
    puts "---- request path ----"
    # puts request.path
    # @current_path = request.path
    @original_url = request.original_url

    if @original_url
      if @original_url.to_s.include?("bcs-edmond.herokuapp.com")
        @original_url.gsub!("https://bcs-edmond.herokuapp.com","https://app.edmond.immo")
      end
    end

    puts @original_url
    if @original_url
      request.env['HTTP_REFERER'] = @original_url
    else
      request.env['HTTP_REFERER'] = request.referer
    end
    puts "----------------------"
  end

end
