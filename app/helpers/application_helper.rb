module ApplicationHelper
  require "base64"

  CONVERT = {
    '\r' => "\r",
    '\t' => "\t",
    '\n' => "\n"
  }
  CONVERT_RX = Regexp.union(CONVERT.keys)

  def sub_cost_percent(sub_year_sum,costs_year_sum)
    ((sub_year_sum.to_f / costs_year_sum.to_f) * 100).round(2)
  end

  def stisla_colors(name)
    datas = {gray: "rgb(80,82,92)", blue: "rgb(103,192,231)", orange: "rgb(250,172,86)", green: "rgb(105,227,130)", red: "rgb(253,84,76)", purple: "rgb(124,142,249)"}
    color = datas[name.to_sym]
    puts color
    color
  end

  def svg_base64_conversion(args={})
    return nil if !args[:name]
    name = args[:name]
    file_path = "#{Rails.root}/app/assets/images/svg/feathers/#{name}.svg"
    css_prefix = "data:image/svg+xml;base64,"
    size = args[:size].to_s
    color = args[:color]
    stroke_width = args[:stroke_width]


    if File.exists?(file_path)
      file_content = File.read(file_path).html_safe

      xml = Nokogiri::XML(file_content)
      svg = xml.at_css("svg")
      if color
        svg.attributes["stroke"].value = color
        svg.set_attribute("fill",color)
      end

      if stroke_width
        svg.set_attribute("stroke-width",stroke_width)
      end

      file_content = svg.to_html.html_safe
      encoded_string = Base64.strict_encode64(file_content)
      result = "background-image: url('#{css_prefix}#{encoded_string}');"
      result = "background-image: url(" + '"' + css_prefix + encoded_string + '"' + ");"
      return result
    else
      return "not found"
    end

  end

  def stripe_invoice_status(status)
    if status == "paid"
      french_status = "Payée"
    else
      french_status = "En attente"
    end
    return french_status
  end

  def credit_card(last4)
    return "**** **** **** #{last4}"
  end

  def expiration_date(month,year)
    return "#{month}/#{year}"
  end

  def application_colors(name)
    datas = {lightblue: "rgb(0,172,217)", yellow: "rgb(255,200,57)", green: "rgb(44,186,127)", bckgray: "rgb(240,243,248)"}
    color = datas[name.to_sym]
    puts color
    color
  end

  def checkbox_with_icon(args={})
    label = args[:label] ? args[:label] : false
    svg_name = args[:svg_name] ? args[:svg_name] : false
    icon = unicon_svg(name: svg_name, size: 25, color: "#6584b7")
    return ("<div class='d-flex d-column ai-center'><div class='svg-container'>#{icon}</div><div class='input-with-icon-label'>#{label}</div></div>").html_safe
  end

  def sanitize_string(str)
    str.gsub(CONVERT_RX, CONVERT).remove("\r","\n","\t").strip
  end

  def find_zipcode_in_string(str)
    str.partition(/\d{5,}/).join(" ")
  end

  def ajax_current_url(args={})
    url = args[:url] ? args[:url] : ""
    return hidden_field :ajax_current_url, :path, value: url, class: "ajax-current-url"
  end

  def french_dates
    ["empty","Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre"]
  end

  def remote_link_to(args={}, &block)
    dismiss = args[:dismiss] ? "data-dismiss='#{args[:dismiss]}'" : false
    toggable = args[:toggable] ? "data-remote-toggable='#{args[:toggable]}'" : false
    pattern = args[:pattern] ? args[:pattern] : "html"
    trackable_url = "data-remote-trackable='#{args[:trackable_url]}'"
    target = args[:target] ? args[:target] : "#"
    html_class = args[:class] ? "class='#{args[:class]}'" : false
    label = args[:label] ? args[:label] : "No label"
    content = block_given? ? capture(&block) : label
    http_method = args[:method] ? args[:method] : "GET"
    dynamic_dom_receiver = args[:dynamic_dom_receiver] ? args[:dynamic_dom_receiver] : ""
    html = ("<a #{html_class if html_class} #{dismiss if dismiss} #{trackable_url} #{toggable if toggable} data-remote-dom-receiver='#{dynamic_dom_receiver}' href='#{target}' data-remote-pattern='#{pattern}' data-remote-link='true' data-http-method='#{http_method}'>#{content}</a>").html_safe
  end

  def svg(name)
    file_path = "#{Rails.root}/app/assets/images/svg/#{name}.svg"
    return File.read(file_path).html_safe if File.exists?(file_path)
    '(not found)'
  end

  def dynamic_svg(args={})
    return nil if !args[:name]
    name = args[:name]
    file_path = "#{Rails.root}/app/assets/images/svg/#{name}.svg"
    file = File.read(file_path).html_safe if File.exists?(file_path)
    xml = Nokogiri::XML(file)
    svg = xml.at_css("svg")
    size = args[:size].to_s
    color = args[:color]
    if color
      all_path = xml.at_css("path")
      all_path.set_attribute("fill",color)
    end

    if size != ""
      svg.attributes["width"].value = size
      svg.attributes["height"].value = size
      return svg.to_html.html_safe
    else
      return File.read(file_path).html_safe if File.exists?(file_path)
      '(not found)'
    end
  end

  def feather_svg(name)
    file_path = "#{Rails.root}/app/assets/images/svg/feathers/#{name}.svg"
    return File.read(file_path).html_safe if File.exists?(file_path)
    '(not found)'
  end

  def stripe_card_svg(name)
    file_path = "#{Rails.root}/app/assets/images/svg/cards/#{name}.svg"
    return File.read(file_path).html_safe if File.exists?(file_path)
    '(not found)'
  end

  def unicon_svg(args={})
    return nil if !args[:name]
    name = args[:name]
    mono = args[:mono]

    if mono
      file_path = "#{Rails.root}/app/assets/images/svg/mono-unicon/#{name}.svg"
    else
      file_path = "#{Rails.root}/app/assets/images/svg/unicon/#{name}.svg"
    end

    file = File.read(file_path).html_safe if File.exists?(file_path)
    xml = Nokogiri::XML(file)
    svg = xml.at_css("svg")
    size = args[:size].to_s
    color = args[:color]

    if color
      all_path = xml.at_css("path")
      all_path.set_attribute("fill",color)
    end

    if size != ""
      svg.attributes["width"].value = size
      svg.attributes["height"].value = size
      return svg.to_html.html_safe
    else
      return File.read(file_path).html_safe if File.exists?(file_path)
      '(not found)'
    end

  end

  def feather_dynamic_svg(args={})
    return nil if !args[:name]
    name = args[:name]
    file_path = "#{Rails.root}/app/assets/images/svg/feathers/#{name}.svg"
    file = File.read(file_path).html_safe if File.exists?(file_path)
    xml = Nokogiri::XML(file)
    svg = xml.at_css("svg")
    size = args[:size].to_s
    color = args[:color]
    stroke_width = args[:stroke_width]

    if color
      svg.attributes["stroke"].value = color
    end

    if stroke_width
      svg.set_attribute("stroke-width",stroke_width)
    end

    if size != ""
      svg.attributes["width"].value = size
      svg.attributes["height"].value = size
      return svg.to_html.html_safe
    else
      return File.read(file_path).html_safe if File.exists?(file_path)
      '(not found)'
    end

  end

  def brands_svg(args={})
    return nil if !args[:name]
    name = args[:name]
    file_path = "#{Rails.root}/app/assets/images/svg/brands/#{name}.svg"
    file = File.read(file_path).html_safe if File.exists?(file_path)
    xml = Nokogiri::XML(file)
    svg = xml.at_css("svg")
    size = args[:size].to_s
    color = args[:color]

    return File.read(file_path).html_safe if File.exists?(file_path)
    '(not found)'

  end

  def generate_chart(args={})
    colors = args[:colors] ? args[:colors] : ""
    height = args[:height] ? args[:height] : ""
    id = args[:id] ? args[:id] : "Identifier is missing"
    duration = args[:duration] ? args[:duration] : "Duration is missing"
    stack = args[:stack] ? args[:stack] : "Stack is missing"
    datas = args[:datas] ? args[:datas] : "Datas are missing"
    labels = args[:labels] ? args[:labels] : "Labels are missing"
    chart_type = args[:chart_type] ? args[:chart_type] : "Chart type is missing"
    canvas = "<canvas id='#{id}' height='#{height}' data-colors='#{colors}' data-chart-type='#{chart_type}' data-chart-stack='#{stack}' data-chart-datas='#{datas}' data-chart-labels='#{labels}'>"
    container = ("<div class='chart-container'>#{canvas}</div>").html_safe
  end

  def month_list_with_values
    [[1,"Jan."],[2,"Fév."],[3,"Mars"],[4,"Avr."],[5,"Mai"],[6,"Juin"],[7,"Jui."],[8,"Août"],[9,"Sep."],[10,"Oct."],[11,"Nov."],[12,"Déc."]]
  end

end
