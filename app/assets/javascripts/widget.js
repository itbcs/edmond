function rangeSlider() {
  var sliders = document.querySelectorAll(".range-slider");
  var firstValue,secondValue,thirdValue = 0;
  console.log(sliders);
  sliders.forEach(function(slider) {
    var range = slider.querySelector("input");
    var label = slider.querySelector("span");
    label.textContent = range.value;
    range.addEventListener("input",function(event) {
      label.textContent = event.currentTarget.value;
    });
  });
}

rangeSlider();
