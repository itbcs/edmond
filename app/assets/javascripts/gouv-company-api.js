var fields = {
  hidden_company_field: ".hidden-company-field",
  hidden_com_name_field: ".hidden-com-name-field",
  hidden_postal_code_field: ".hidden-postal-code-field",
  hidden_city_field: ".hidden-city-field",
  hidden_siret_field: ".hidden-siret-field",
  hidden_juridical_nature: ".hidden-juridical-nature-field",
  hidden_siren_field: ".hidden-siren-field",
  hidden_activity_start_date_field: ".hidden-activity-start-date-field",
  hidden_full_address_field: ".hidden-full-address-field",
  hidden_juridical_nature_code: ".hidden-juridical-nature-code-field"
}

var whitelist = ["className","childNodes","textContent"];
function domToObj (domEl) {
  var obj = {};
  for (let i=0; i<whitelist.length; i++) {
    if (domEl[whitelist[i]] instanceof NodeList) {
      obj[whitelist[i]] = Array.from(domEl[whitelist[i]]);
    }
    else {
      obj[whitelist[i]] = domEl[whitelist[i]];
    }
  };
  return obj;
}

function fieldWithApiRequest(inputValue) {

  var isnum = /^\d+$/.test(inputValue);
  if (isnum) {
    console.log("Number query");
    var requestUrl = "https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/" + inputValue;
  } else {
    console.log("Normal query");
    var requestUrl = "https://entreprise.data.gouv.fr/api/sirene/v1/full_text/" + inputValue + "?page=1&per_page=10";
  }

  // var requestUrl = "https://entreprise.data.gouv.fr/api/sirene/v1/full_text/" + inputValue + "?page=1&per_page=10";
  var request = new XMLHttpRequest();
  request.open('GET',requestUrl);
  request.responseType = 'json';
  request.send();

  request.onload = function() {
    var status = request.status;
    var requestReturnJson = request.response;
    var resultSize = request.response.total_results;
    var companies = request.response.etablissement;
    var receiver = document.querySelector(".result-container");

    // console.info(companies);
    // console.info(request.response);
    if (isnum && companies) {
      receiver.innerHTML = "";
      iterateOverCompanies([companies],receiver,resultSize,"v3Request");
      addItemDatasToField();
    } else if (companies) {
      receiver.innerHTML = "";
    } else {
      receiver.innerHTML = "";
      addEmptyResultToReceiver(receiver);
    }

    if (status == 404) {
      if (request.response.suggestions) {
        var suggestion = request.response.suggestions[0];
      } else {
        var suggestion = null;
      }
      console.table(request.response.suggestions);
      if (suggestion) {
        console.info("There is suggestions");
        fieldWithApiRequest(suggestion);
      } else {
        // addEmptyResultToReceiver(receiver);
      }
    } else {
      iterateOverCompanies(companies,receiver,resultSize,"v1Request");
      addItemDatasToField();
    }
  }
}

var input = document.querySelector("input.test");
var icon = generateIcon();
if (input) {
  input.addEventListener("keyup", function(event) {
    var receiver = document.querySelector(".result-container");
    if (this.value.length > 1) {
      console.log("Value superior to 1");
      fieldWithApiRequest(this.value);
    } else {
      receiver.classList.add("d-none");
    }
  });
}

function addItemDatasToField() {
  var items = document.querySelectorAll(".item");
  items.forEach(function(i) {
    i.addEventListener("click",function(event) {
      var nodeToJson = JSON.stringify(i, ["textContent", "className", "childNodes"]);
      var result = handleNodesToObject(i);

      companyInput = document.querySelector(fields.hidden_company_field);
      comNameInput = document.querySelector(fields.hidden_com_name_field);
      postalCodeInput = document.querySelector(fields.hidden_postal_code_field);
      cityInput = document.querySelector(fields.hidden_city_field);
      siretInput = document.querySelector(fields.hidden_siret_field);
      juridicalNatureInput = document.querySelector(fields.hidden_juridical_nature);
      sirenInput = document.querySelector(fields.hidden_siren_field);
      activityStartDateInput = document.querySelector(fields.hidden_activity_start_date_field);
      fullAddressInput = document.querySelector(fields.hidden_full_address_field);

      companyInputValue = result.childNodes[1].textContent;
      comNameInputValue = result.childNodes[6].textContent;
      postalCodeInputValue = result.childNodes[2].textContent;
      cityInputValue = result.childNodes[3].textContent;
      siretInputValue = result.childNodes[5].textContent;
      juridicalNatureInputValue = result.childNodes[7].textContent;
      sirenInputValue = result.childNodes[4].textContent
      activityStartDateInputValue = result.childNodes[8].textContent
      fullAddressInputValue = result.childNodes[9].textContent

      if (companyInputValue === "Enseigne:Non renseigné") {
        console.log("Valeur introuvable");
        companyInputValue = sliceString(comNameInputValue);
      }

      companyInput.value = sliceString(companyInputValue);
      comNameInput.value = sliceString(comNameInputValue);
      postalCodeInput.value = sliceString(postalCodeInputValue);
      cityInput.value = sliceString(cityInputValue);
      siretInput.value = sliceString(siretInputValue);
      juridicalNatureInput.value = sliceString(juridicalNatureInputValue);
      sirenInput.value = sliceString(sirenInputValue);
      activityStartDateInput.value = sliceString(activityStartDateInputValue);
      fullAddressInput.value = sliceString(fullAddressInputValue);

      var input = document.querySelector("input.test");
      input.value = sliceString(companyInputValue) + " - " + sliceString(postalCodeInputValue) + " - " + sliceString(cityInputValue) + " - " + sliceString(siretInputValue);
      var receiver = document.querySelector(".result-container");
      receiver.classList.add("d-none");
    });
  });
}

function sliceString(str) {
  var slicer = ":";
  var slicingResult = str.substr(str.indexOf(slicer) + slicer.length);
  return slicingResult;
}

function handleNodesToObject(domElement) {
  result = JSON.stringify(domElement, function (name, value) {
      if (name === "") {
        return domToObj(value);
      }
      if (Array.isArray(this)) {
          if (typeof value === "object") {
            return domToObj(value);
          }
          return value;
      }
      if (whitelist.find(x => (x === name)))
          return value;
  });
  return JSON.parse(result);
}


function iterateOverCompanies(companies,receiver,resultSize,requestOrigin) {
  // var resultHeader = document.createElement('div');
  // resultHeader.classList.add("result-header");
  // var resultText = document.createTextNode(resultSize.toLocaleString() + " Résultats");
  // resultHeader.appendChild(resultText);
  // receiver.appendChild(resultHeader);

  Array.from(companies).forEach(function(item) {
      console.log(item);

      var rawData = document.createElement("p");
      rawData.innerHTML = JSON.stringify(item);

      var htmlObject = document.createElement('div');
      htmlObject.classList.add("item");

      var companyName = document.createElement("div");
      companyName.classList.add("company-name");
      var label = document.createElement("p");
      label.innerHTML = "Enseigne:";
      companyName.appendChild(label);

      if (requestOrigin == "v3Request") {
        companyName.appendChild(document.createTextNode(item.unite_legale.denomination));
      } else if (requestOrigin == "v1Request") {
        if (item.l1_normalisee == null) {
          companyName.appendChild(document.createTextNode("Non renseigné"));
        } else {
          companyName.appendChild(document.createTextNode(item.l1_normalisee));
        }
      }


      if (requestOrigin == "v3Request") {
        var companyComName = document.createElement("div");
        companyComName.classList.add("company-com-name");
        var label = document.createElement("p");
        label.innerHTML = "Raison sociale:";
        companyComName.appendChild(label);
        companyComName.appendChild(document.createTextNode(item.enseigne_1));
      } else if (requestOrigin == "v1Request") {
        var companyComName = document.createElement("div");
        companyComName.classList.add("company-com-name");
        var label = document.createElement("p");
        label.innerHTML = "Raison sociale:";
        companyComName.appendChild(label);
        companyComName.appendChild(document.createTextNode(item.enseigne));
      }


      var companyPostalCode = document.createElement("div");
      companyPostalCode.classList.add("company-postal-code");
      var label = document.createElement("p");
      label.innerHTML = "Code postal:";
      companyPostalCode.appendChild(label);
      companyPostalCode.appendChild(document.createTextNode(item.code_postal));

      var companyCity = document.createElement("div");
      companyCity.classList.add("company-city");
      var label = document.createElement("p");
      label.innerHTML = "Commune:";
      companyCity.appendChild(label);
      companyCity.appendChild(document.createTextNode(item.libelle_commune));

      var companySiret = document.createElement("div");
      companySiret.classList.add("company-siret");
      var label = document.createElement("p");
      label.innerHTML = "Siret:";
      companySiret.appendChild(label);
      companySiret.appendChild(document.createTextNode(item.siret));

      if (requestOrigin == "v3Request") {
        var companyJuridicalNature = document.createElement("div");
        companyJuridicalNature.classList.add("company-juridical-nature");
        var label = document.createElement("p");
        label.innerHTML = "Nature juridique:";
        companyJuridicalNature.appendChild(label);
        companyJuridicalNature.appendChild(document.createTextNode(item.unite_legale.categorie_juridique));
      } else if (requestOrigin == "v1Request") {
        var companyJuridicalNature = document.createElement("div");
        companyJuridicalNature.classList.add("company-juridical-nature");
        var label = document.createElement("p");
        label.innerHTML = "Nature juridique:";
        companyJuridicalNature.appendChild(label);
        companyJuridicalNature.appendChild(document.createTextNode(item.nature_juridique_entreprise));
      }

      var companySiren = document.createElement("div");
      companySiren.classList.add("company-siren");
      var label = document.createElement("p");
      label.innerHTML = "Siren:";
      companySiren.appendChild(label);
      companySiren.appendChild(document.createTextNode(item.siren));

      if (requestOrigin == "v3Request") {
        var companyActivityStartDate = document.createElement("div");
        companyActivityStartDate.classList.add("company-activity-start-date");
        var label = document.createElement("p");
        label.innerHTML = "Début d'actvité:";
        companyActivityStartDate.appendChild(label);
        companyActivityStartDate.appendChild(document.createTextNode(item.unite_legale.date_debut));
      } else if (requestOrigin == "v1Request") {
        var companyActivityStartDate = document.createElement("div");
        companyActivityStartDate.classList.add("company-activity-start-date");
        var label = document.createElement("p");
        label.innerHTML = "Début d'actvité:";
        companyActivityStartDate.appendChild(label);
        companyActivityStartDate.appendChild(document.createTextNode(item.date_debut_activite));
      }

      var companyFullAddress = document.createElement("div");
      companyFullAddress.classList.add("company-full-address");
      var label = document.createElement("p");
      label.innerHTML = "Adresse complète:";
      companyFullAddress.appendChild(label);
      companyFullAddress.appendChild(document.createTextNode(item.geo_adresse));

      // console.table(item);
      // console.table({full_address: item.geo_adresse});

      var iconContainer = document.createElement('div');
      iconContainer.classList.add("search-icon");
      iconContainer.innerHTML = icon;
      htmlObject.appendChild(iconContainer);

      htmlObject.appendChild(companyName);
      htmlObject.appendChild(companyPostalCode);
      htmlObject.appendChild(companyCity);
      htmlObject.appendChild(companySiren);
      htmlObject.appendChild(companySiret);
      htmlObject.appendChild(companyComName);
      htmlObject.appendChild(companyJuridicalNature);
      htmlObject.appendChild(companyActivityStartDate);
      htmlObject.appendChild(companyFullAddress);

      receiver.appendChild(htmlObject);
      receiver.classList.remove("d-none");
    });
}

function addEmptyResultToReceiver(receiver) {
  var htmlObject = document.createElement('div');
  htmlObject.classList.add("item");
  var errorMessage = document.createElement("div");
  errorMessage.classList.add("error-message");
  var messageText = document.createTextNode("Aucun résultat");
  errorMessage.appendChild(messageText);
  htmlObject.appendChild(errorMessage);
  receiver.appendChild(htmlObject);
}

function generateIcon() {
  svg = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill="#00acd9" d="M10,6V5h4V6h2V5a2.00229,2.00229,0,0,0-2-2H10A2.00229,2.00229,0,0,0,8,5V6Z"/><path fill="#99ddef" d="M9 15a.99974.99974 0 0 1-1-1V12a1 1 0 0 1 2 0v2A.99974.99974 0 0 1 9 15zM15 15a.99974.99974 0 0 1-1-1V12a1 1 0 0 1 2 0v2A.99974.99974 0 0 1 15 15z"/><path fill="#66cde8" d="M20,6H4A2,2,0,0,0,2,8v3a2,2,0,0,0,2,2H8V12a1,1,0,0,1,2,0v1h4V12a1,1,0,0,1,2,0v1h4a2,2,0,0,0,2-2V8A2,2,0,0,0,20,6Z"/><path fill="#00acd9" d="M20,13H16v1a1,1,0,0,1-2,0V13H10v1a1,1,0,0,1-2,0V13H4a2,2,0,0,1-2-2v8a2,2,0,0,0,2,2H20a2,2,0,0,0,2-2V11A2,2,0,0,1,20,13Z"/></svg>';
  return svg;
}

function itemClickEvent(event) {
  var clickedItem = event.currentTarget;
}

function startUserSignin() {
  var input = document.querySelector("input.test");
  var icon = generateIcon();

  if (input) {
    input.addEventListener("keyup", function(event) {
      var receiver = document.querySelector(".result-container");
      receiver.innerHTML = "";
      if (this.value.length > 1) {
        console.log("Value superior to 1");
        fieldWithApiRequest(this.value);
      } else {
        receiver.classList.add("d-none");
      }
    });
  }
}









