function handleCheckboxInputSwapper() {
  var monthlyInput = document.querySelector("input#subscription_recurrence_1");
  var annualInput = document.querySelector("input#subscription_recurrence_2");
  if (monthlyInput && annualInput) {
    var amountInputContainer = document.querySelector(".input-swapper");
    if (amountInputContainer) {
      var hint = amountInputContainer.querySelector(".hint");
      var amountInput = amountInputContainer.querySelector("input#subscription_amount");
      monthlyInput.addEventListener("change", function() {
        if (monthlyInput.checked) {
          amountInput.setAttribute("placeholder", "Coût / mois");
          hint.textContent = "€ HT / Mois";
        }
      });
       annualInput.addEventListener("change", function() {
        if (annualInput.checked) {
          amountInput.setAttribute("placeholder", "Coût / an");
          hint.textContent = "€ HT / An";
        }
      });
    }
  }
}
