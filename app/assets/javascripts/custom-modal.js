function modalLogic() {
  var modalTriggers = document.querySelectorAll("[data-custom-trigger='custom-modal']");
  if (modalTriggers.length > 0) {
    modalTriggers.forEach(function(trigger) {
      var target = document.querySelector(trigger.dataset.customTarget);
      var targetClosers = target.querySelectorAll("[data-close='custom-modal']");

      trigger.addEventListener("click",function(event) {
        console.log("Custom modal trigger clicked !!");
        target.classList.remove("d-none");
      });

      targetClosers.forEach(function(closer) {
        closer.addEventListener("click",function() {
          target.classList.add("d-none");
        });
      });

    });
  }
}

modalLogic();
