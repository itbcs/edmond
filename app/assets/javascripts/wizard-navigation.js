function wizardNavigation() {
  findFileInputs();
  var submitButtons = document.querySelectorAll("input[type='submit'].wizard-submit");
  submitButtons.forEach(function(item) {
    item.addEventListener("click",function(event) {
      event.preventDefault();
      var receiver = document.querySelector(".wizard-dynamic-content");
      var form = document.querySelector("form.new_user");
      var formDatas = new FormData(form);
      var url = form.getAttribute("action");
      var method = form.getAttribute("method");
      if (this.getAttribute("name") === "back_button") {
        formDatas.append("back_button", "retour");
      }
      wizardAjaxRequest(method,url,"html",receiver,formDatas);
    });
  });
}


function wizardAjaxRequest(method,url,pattern,receiver,data) {
  $.ajax({
    url: url,
    processData: false,
    contentType: false,
    data: data,
    type: method,
    beforeSend: function(event) {
    },
    success: function(data) {
      console.log("---- inside wizardAjaxRequest ajax success----")
      console.log(data);
      if (data.url) {
        window.location = data.url;
      } else {
        AjaxResponder[pattern + "Response"](data,receiver);
      }
    },
    error: function(data) {
      AjaxResponder["errorResponse"](data);
    },
    complete: function(data) {
      wizardNavigation();
      radiobuttonsJourney();
      startUserSignin();
    }
  });
}

$(document).ready(function() {
  wizardNavigation();
})
