function findFileInputs() {
  var file;
  var fileInputs = document.querySelectorAll(".custom-file-input-container");
  if (fileInputs.length !== 0) {
    fileInputs.forEach(function(item) {
      item.querySelector("input[type=file]").addEventListener("change",function(event) {
        file = item.querySelector("input[type=file]").files[0];
        var reader = new FileReader();
        reader.addEventListener("load",function(event2) {
          if (file.type == "application/pdf") {
            var fileType = "Pdf";
          } else if (file.type == "image/jpeg") {
            var fileType = "Jpeg";
          } else if (file.type == "image/png") {
            var fileType = "Png";
          }

          if (item.classList.contains("done")) {
            item.classList.remove("done");
          } else if (item.classList.contains("empty")) {
            item.classList.remove("empty");
          }

          item.classList.add("ongoing");
          item.querySelector("h5").textContent = file.name;
          item.querySelector("p").textContent = "Taille: " + (file.size/1000000).toFixed(2) + " Mb" + " / Type: " + fileType;
        }, false);
        if (file) {
          reader.readAsDataURL(file);
          // uploadFile(file,item);
          addPhoto(file,item);
        }
      });
    });
  }
}





