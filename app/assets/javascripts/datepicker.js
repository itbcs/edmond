$(document).ready(function() {
  $.fn.datepicker.dates['fr'] = {
    days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
    daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
    daysMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
    months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
    monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Juil", "Aoû", "Sep", "Oct", "Nov", "Dec"],
    today: "Aujourd'hui",
    clear: "Effacer",
    format: "dd/mm/yyyy",
    titleFormat: "MM yyyy",
    weekStart: 0
  };
});


function getDatepickers() {
  var options = {
    language: "fr-FR",
    daysOfWeekDisabled: "0"
  }
  var pickers = document.querySelectorAll("[data-date-picker]");
  if (pickers) {
    pickers.forEach(function(picker) {
      var pickerType = picker.dataset.datePickerType;
      if (pickerType) {
        options = {
          language: "fr-FR",
          format: "yyyy",
          minViewMode: "years"
        }
      }
      $(picker).datepicker(options);
    });
  }
}
