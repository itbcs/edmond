$(document).ready(function() {
  $('#user_business_card').mask('CPI 0000 0000 000 000 000');
  $('#user_phone').mask('00 00 00 00 00');
  $('#user_companies_attributes_0_balance_closing_date').mask("00/00/0000");
});

function getAllMaskInputs() {
  var inputs = document.querySelectorAll("[data-remote-mask]");
  if (inputs) {
    inputs.forEach(function(item) {
      $(item).mask("# ##0", {reverse: true});
    })
  }
}
