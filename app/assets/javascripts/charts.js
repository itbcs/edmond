Chart = window.Chart;
var colors = {
  yellow: "#ffc839",
  lightyellow: "#ffe49f",
  blue: "#00acd9",
  lightblue: "#5addff",
  grayblue: "#4d7992",
  lightgrayblue: "#86aabf",
  rgbBlue: "rgba(20,160,210,0.5)"
}

$(document).ready(function() {
  Chart.elements.Rectangle.prototype.draw = function () {
    var ctx = this._chart.ctx;
    var vm = this._view;
    var left, right, top, bottom, signX, signY, borderSkipped, radius;
    var borderWidth = vm.borderWidth;
    var cornerRadius = 12;

    if (!vm.horizontal) {
      left = vm.x - vm.width / 2;
      right = vm.x + vm.width / 2;
      top = vm.y;
      bottom = vm.base;
      signX = 1;
      signY = bottom > top ? 1 : -1;
      borderSkipped = vm.borderSkipped || 'bottom';
    } else {
      left = vm.base;
      right = vm.x;
      top = vm.y - vm.height / 2;
      bottom = vm.y + vm.height / 2;
      signX = right > left ? 1 : -1;
      signY = 1;
      borderSkipped = vm.borderSkipped || 'left';
    }

    if (borderWidth) {
      var barSize = Math.min(Math.abs(left - right), Math.abs(top - bottom));
      borderWidth = borderWidth > barSize ? barSize : borderWidth;
      var halfStroke = borderWidth / 2;
      var borderLeft = left + (borderSkipped !== 'left' ? halfStroke * signX : 0);
      var borderRight = right + (borderSkipped !== 'right' ? -halfStroke * signX : 0);
      var borderTop = top + (borderSkipped !== 'top' ? halfStroke * signY : 0);
      var borderBottom = bottom + (borderSkipped !== 'bottom' ? -halfStroke * signY : 0);
      if (borderLeft !== borderRight) {
        top = borderTop;
        bottom = borderBottom;
      }
      if (borderTop !== borderBottom) {
        left = borderLeft;
        right = borderRight;
      }
    }

    ctx.beginPath();
    ctx.fillStyle = vm.backgroundColor;
    ctx.strokeStyle = vm.borderColor;
    ctx.lineWidth = borderWidth;
    var corners = [
      [left, bottom],
      [left, top],
      [right, top],
      [right, bottom]
    ];

    var borders = ['bottom', 'left', 'top', 'right'];
    var startCorner = borders.indexOf(borderSkipped, 0);
    if (startCorner === -1) {
      startCorner = 0;
    }

    function cornerAt(index) {
      return corners[(startCorner + index) % 4];
    }

    var corner = cornerAt(0);
    var width, height, x, y, nextCorner, nextCornerId
    var x_tl, x_tr, y_tl, y_tr;
    var x_bl, x_br, y_bl, y_br;
    ctx.moveTo(corner[0], corner[1]);

    for (var i = 1; i < 4; i++) {
      corner = cornerAt(i);
      nextCornerId = i + 1;
      if (nextCornerId == 4) {
        nextCornerId = 0
      }
      nextCorner = cornerAt(nextCornerId);
      width = corners[2][0] - corners[1][0];
      height = corners[0][1] - corners[1][1];
      x = corners[1][0];
      y = corners[1][1];
      radius = cornerRadius;
      if (radius > Math.abs(height) / 2) {
        radius = Math.floor(Math.abs(height) / 2);
      }
      if (radius > Math.abs(width) / 2) {
        radius = Math.floor(Math.abs(width) / 2);
      }
      if (height < 0) {
        x_tl = x; x_tr = x + width;
        y_tl = y + height; y_tr = y + height;
        x_bl = x; x_br = x + width;
        y_bl = y; y_br = y;
        ctx.moveTo(x_bl + radius, y_bl);
        ctx.lineTo(x_br - radius, y_br);
        ctx.quadraticCurveTo(x_br, y_br, x_br, y_br - radius);
        ctx.lineTo(x_tr, y_tr + radius);
        ctx.quadraticCurveTo(x_tr, y_tr, x_tr - radius, y_tr);
        ctx.lineTo(x_tl + radius, y_tl);
        ctx.quadraticCurveTo(x_tl, y_tl, x_tl, y_tl + radius);
        ctx.lineTo(x_bl, y_bl - radius);
        ctx.quadraticCurveTo(x_bl, y_bl, x_bl + radius, y_bl);
      } else if (width < 0) {
        x_tl = x + width; x_tr = x;
        y_tl = y; y_tr = y;
        x_bl = x + width; x_br = x;
        y_bl = y + height; y_br = y + height;
        ctx.moveTo(x_bl + radius, y_bl);
        ctx.lineTo(x_br - radius, y_br);
        ctx.quadraticCurveTo(x_br, y_br, x_br, y_br - radius);
        ctx.lineTo(x_tr, y_tr + radius);
        ctx.quadraticCurveTo(x_tr, y_tr, x_tr - radius, y_tr);
        ctx.lineTo(x_tl + radius, y_tl);
        ctx.quadraticCurveTo(x_tl, y_tl, x_tl, y_tl + radius);
        ctx.lineTo(x_bl, y_bl - radius);
        ctx.quadraticCurveTo(x_bl, y_bl, x_bl + radius, y_bl);
      } else {
        ctx.moveTo(x + radius, y);
        ctx.lineTo(x + width - radius, y);
        ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
        ctx.lineTo(x + width, y + height - radius);
        ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
        ctx.lineTo(x + radius, y + height);
        ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
        ctx.lineTo(x, y + radius);
        ctx.quadraticCurveTo(x, y, x + radius, y);
      }
    }
    ctx.fill();
    if (borderWidth) {
      ctx.stroke();
    }
  };
});



function findAllchartsChanger(chart,identifier,data,options,ctx) {
  var trigger = document.querySelector("[data-chart-target='" + identifier + "']");
  if (trigger) {
    trigger.addEventListener("click",function(event) {
      var type = this.dataset.chartType;
      chart.destroy();
      chart = new Chart(ctx, {
        type: type,
        data: data,
        options: options
      });
    });
  }
}

function findAllCharts() {
  charts = document.querySelectorAll(".chart-container");
  charts.forEach(function(item) {
    var canvas = item.querySelector("canvas");
    var id = canvas.id;
    var chartType = canvas.dataset.chartType;
    var datas = JSON.parse(canvas.dataset.chartDatas);
    var labels = JSON.parse(canvas.dataset.chartLabels);

    if (canvas.dataset.colors) {
      var colors = JSON.parse(canvas.dataset.colors);
    } else {
      var colors = undefined;
    }


    var stack = canvas.dataset.chartStack;
    var resume = {id: id, chart_type: chartType, datas: datas, stack: stack};
    if (stack === "true") {
      chartDatas = generateStackDatas(datas,labels);
    } else {
      chartDatas = generateDatas(datas,labels);
    }
    if (chartType == "doughnut") {
      chartOptions = generateDoughnutOptions();
      chartDatas = generateDoughnutDatas(datas,labels,colors);
    } else if (chartType == "line") {
      chartOptions = generateOptions();
      chartDatas = generateLineDatas(datas,labels);
    } else {
      chartOptions = generateOptions();
    }
    generateChart(chartType,id,chartDatas,chartOptions);
  });
}

function generateChart(chartType,identifier,data,options) {
  var ctx = document.getElementById(identifier).getContext('2d');
  var chart = new Chart(ctx, {
    type: chartType,
    data: data,
    plugins: [window.ChartDataLabels],
    options: options
  });
  findAllchartsChanger(chart,identifier,data,options,ctx);
}

function generateDoughnutDatas(datas,labels,colors) {
  if (!colors) {
    var colors = ["rgb(255,200,57)","rgba(255,200,57,0.3)"];
  }
  var data = {
    labels: labels,
    datasets: [{
      data: datas,
      backgroundColor: colors,
      borderColor: "transparent"
    }]
  };
  return data;
}

function generateLineDatas(datas,labels) {
  var data = {
    labels: labels,
    datasets: [{
      data: datas,
      backgroundColor: colors.blue,
      fill: false,
      borderColor: colors.blue,
    }]
  };
  return data;
}

function generateDatas(datas,labels) {
  var data = {
    labels: labels,
    datasets: [{
      data: datas,
      borderWidth: 1,
      pointRadius: 3,
      pointBackgroundColor: 'rgba(70,150,240,1)',
      backgroundColor: function(context) {
        var index = context.dataIndex;
        var size = Object.keys(context.dataset.data).length;
        var middle = size/2;
        middle = Math.ceil(middle);
        var index = context.dataIndex;
        var color;

        if (index < middle) {
          color = colors.blue;
        } else if (index >= middle) {
          color = colors.lightblue;
        }
        return color;
      },
      barThickness: parseInt(16),
      borderColor: "transparent"
    }]
  };
  return data;
}

function generateStackDatas(datas,labels) {
  var labels = labels;
  var data = {
    labels: labels,
    datasets: [{
      label: "Fees",
      barPercentage: 0.4,
      backgroundColor: function(context) {
        var size = Object.keys(context.dataset.data).length;
        var index = context.dataIndex;
        var color;
        var currentMonth = new Date().getMonth() + 1;
        if (index < currentMonth) {
          color = colors.blue;
        } else if (index >= currentMonth) {
          color = colors.lightblue;
        }
        return color;
      },
      data: datas[0],
    }, {
      label: "Pending Fees",
      barPercentage: 0.4,
      backgroundColor: colors.lightblue,
      data: datas[2]
    }, {
      stack: "Costs",
      barPercentage: 0.4,
      backgroundColor: function(context) {
        var size = Object.keys(context.dataset.data).length;
        var index = context.dataIndex;
        var color;
        var currentMonth = new Date().getMonth() + 1;
        if (index < currentMonth) {
          color = colors.yellow;
        } else if (index >= currentMonth) {
          color = colors.lightyellow;
        }
        return color;
      },
      data: datas[1]
    }]
  };
  return data;
}


function generateDoughnutOptions() {
  var options = {
    cutoutPercentage: 70,
    responsive: true,
    tooltips: {
      enabled: false,
      display: false
    },
    legend: {
      display: false
    },
    title: {
      display: false
    },
    plugins: {
      datalabels: {
        display: false
      }
    },
  }
  return options;
}


function generateOptions() {
  var options = {
    responsive: true,
    layout: {
      padding: {
      top: 30,
      bottom: 0,
      right: 30,
      left: 0
    }
  },
  plugins: {
    datalabels: {
      anchor: "end",
      clamp: true,
      align: "top",
      textAlign: "center",
      clip: false,
      offset: 0,
      formatter: function(value, context) {
        if (value) {
          return value.toLocaleString("fr-FR");
        }
      },
      font: function(context) {
        var index = context.dataIndex;
        var value = context.dataset.data[index];
        var size = 14;
        return {
          size: size,
          weight: 700,
          family: "Source Sans Pro"
        };
      },
      color: "rgb(70,70,75)"
    }
  },
  tooltips: {
    enabled: false,
    backgroundColor: "rgba(255,255,255,1)",
    borderColor: "rgba(200,200,200,1)",
    yAlign: "center",
    xAlign: "center",
    borderWidth: 0.5,
    titleFontFamily: "Source Sans Pro",
    bodyFontFamily: "Source Sans Pro",
    footerFontFamily: "Source Sans Pro",
    titleFontColor: "rgba(100,100,100,1)",
    bodyFontColor: "rgba(100,100,100,1)",
    footerFontColor: "rgba(100,100,100,1)",
    bodyFontStyle: 400,
    bodySpacing: 0,
    titleSpacing: 0,
    xPadding: 18,
    yPadding: 10,
    bodyFontSize: 12
  },
  legend: {
    display: false
  },
  scales: {
    yAxes: [{
      stacked: true,
      gridLines: {
        borderDash: [2,4],
        color: "rgb(255,255,255)",
        zeroLineWidth: 1,
        zeroLineColor: "rgb(220,220,230)",
        zeroLineBorderDash: [2,4],
        tickMarkLength: 0,
        lineWidth: 1,
        offsetGridLines: false,
        drawBorder: false
      },
      display: true,
      ticks: {
        callback: function(value, index, values) {
          return value + "      ";
        },
        beginAtZero: true,
        fontFamily: "Source Sans Pro",
        fontColor: "rgb(255,255,255)",
        fontStyle: 500,
        fontSize: 13,
        yPadding: 10
      },
      scaleLabel: {
        display: false,
        labelString: "Example",
        fontColor: "rgb(170,190,210)",
        fontFamily: "Source Sans Pro"
      }
    }],
    xAxes: [{
      stacked: true,
      gridLines: {
        borderDash: [5,15],
        display: false,
        drawBorder: false,
        offsetGridLines: true,
        zeroLineWidth: 0
      },
      display: true,
      ticks: {
        beginAtZero: true,
        display: true,
        fontColor: "rgb(134,170,191)",
        fontStyle: 600,
        fontSize: 12,
        fontFamily: "Source Sans Pro"
      }
    }]
  }
  }
  return options;
}




