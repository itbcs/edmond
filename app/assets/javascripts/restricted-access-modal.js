function handleRestrictedModal() {
  var customModal = document.getElementById("restricted-account");
  if (customModal) {
    customModal.classList.remove("d-none");
  }
}


document.addEventListener("DOMContentLoaded",function(){
   handleRestrictedModal();
});
