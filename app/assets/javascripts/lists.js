function getAllLists() {
  var lists = document.querySelectorAll("[data-remote-list]");
  if (lists) {
    lists.forEach(function(list) {
      var elementId = list.dataset.remoteList;
      var values = JSON.parse(list.dataset.remoteValues);
      generateList(elementId,values);
    });
  }
}

function generateList(id,values) {
  var options = {
    valueNames: values,
    pagination: true,
    page: 7
  };
  new List(id,options);
}
