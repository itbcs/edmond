function radiobuttonsJourney() {
  var buttons = document.querySelectorAll("input.shareholder");
  var genuineActive = document.querySelector("section.shareholder");
  var genuinePassive = document.querySelector("section.passive-shareholder");
  var form = document.querySelector(".dynamic-form-container");

  if (genuineActive) {
    var activeClone = copyDomObject(genuineActive);
  }
  if (genuinePassive) {
    var passiveClone = copyDomObject(genuinePassive);
  }

  if (genuineActive) {
    genuineActive.parentNode.removeChild(genuineActive);
  }

  if (genuinePassive) {
    genuinePassive.parentNode.removeChild(genuinePassive);
  }

  if (buttons) {
    buttons.forEach(function(button) {
      button.addEventListener("click",function(event) {
        var domTarget = button.dataset.domTarget;
        if (domTarget == "section.passive-shareholder") {
          var previousChoice = document.querySelector("section.shareholder");
          if (previousChoice) {
            previousChoice.parentNode.removeChild(previousChoice);
          }
          form.appendChild(passiveClone);
        } else if (domTarget == "section.shareholder") {
          var previousChoice = document.querySelector("section.passive-shareholder");
          if (previousChoice) {
            previousChoice.parentNode.removeChild(previousChoice);
          }
          form.appendChild(activeClone);
        }
      });
    });
  }
}

function copyDomObject(node) {
  var clonedNode = node.cloneNode(true);
  return clonedNode;
}
