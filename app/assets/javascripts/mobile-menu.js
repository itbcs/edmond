function mobileMenu() {
  var menuTrigger = document.querySelector(".mobile-btn-trigger");
  if (menuTrigger) {
    menuTrigger.addEventListener("click", function() {
      console.log("Mobile menu trigger click");
      var menuContent = document.querySelector(".mobile-menu-content");
      if (menuContent) {
        menuContent.classList.remove("d-none");
        var menuCloser = menuContent.querySelector(".mobile-menu-closer");
        menuCloser.addEventListener("click", function() {
          menuContent.classList.add("d-none");
        });
      }
    });
  }
}

document.addEventListener("DOMContentLoaded",function(){
  mobileMenu();
});
