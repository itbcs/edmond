function getCurrentUrl() {
  currentPath = document.querySelectorAll(".ajax-current-url");
  return currentPath[currentPath.length - 1];
}

function getAllLinks() {
  return document.querySelectorAll("[data-remote-link='true']");
}

function getAllRemotedForms() {
  var forms = document.querySelectorAll("[data-remote-form='true']");
  if (forms) {
    forms.forEach(function(form) {
      form.removeEventListener("submit",formSubmit);
      form.addEventListener("submit",formSubmit);
    });
  }
}

function formSubmit(event) {
  event.preventDefault();
  var target = event.currentTarget;
  var url = target.getAttribute("action");
  var method = target.getAttribute("method");
  var formDatas = new FormData(target);
  var eventType = "submit";
  var trackable = "false"
  var receiver = document.querySelector(target.dataset.dynamicReceiver);
  var keepBackdrop = target.dataset.keepBackdrop;
  ajaxRequest(method,url,"html",receiver,"true",trackable,formDatas);
}


function startDashboard() {
  trigger = document.querySelector("input[type='hidden'].start-dashboard-trigger");
  if (trigger) {
    url = trigger.value;
    receiverIdentifier = trigger.dataset.remoteDomReceiver;
    console.info("Start dashboard 🧨");
    var receiver = document.querySelector(receiverIdentifier);
    var eventType = "click";
    var trackable = "true";
    ajaxRequest("GET",url,"html",receiver,"false",trackable);
  }
}

function ajaxNavigation(links) {
  links.forEach(function(item) {
    item.removeEventListener("click",linkClick);
    item.addEventListener("click",linkClick);
  });
}

function toggableLink(currentLink,identifier) {
  document.querySelectorAll(identifier).forEach(item => {
    item.classList.remove("active");
  });
  currentLink.querySelector(identifier).classList.add("active");
}


function linkClick(event) {
  event.preventDefault();
  var target = event.currentTarget;
  httpMethod = target.dataset.httpMethod;
  toggableIdentifier = target.dataset.remoteToggable;
  nested = target.dataset.remoteNested;
  domReceiverIdentifier = target.dataset.remoteDomReceiver;
  var trackable = target.dataset.remoteTrackable;


  if (domReceiverIdentifier) {
    receiver = document.querySelector(domReceiverIdentifier);
  } else {
    receiver = undefined;
  }

  var eventType = "click";
  url = target.href;
  currentUrl = getCurrentUrl();
  pattern = target.dataset.remotePattern;
  rawDatas = {url: url, http_method: httpMethod, pattern: pattern};
  if (toggableIdentifier) {
    toggableLink(target,toggableIdentifier);
  }
  if (currentUrl) {
    if (currentUrl.value == url) {
      console.log("Your current page is already loading !!!!");
    } else {
      ajaxRequest(httpMethod,url,pattern,receiver,nested,trackable);
    }
  } else {
    ajaxRequest(httpMethod,url,pattern,receiver,nested,trackable);
  }
}

function ajaxRequest(method,url,pattern,receiver,nested,trackable,data) {
  if (data === undefined) data = "";
  $.ajax({
    url: url,
    processData: false,
    contentType: false,
    data: data,
    type: method,
    beforeSend: function(event) {
      loadingEvent(receiver);
    },
    success: function(data) {
      AjaxResponder[pattern + "Response"](data,receiver,url,trackable);
    },
    error: function(data) {
      AjaxResponder["errorResponse"](data);
    },
    complete: function(data) {
      ajaxNavigation(getAllLinks());
      findAllCharts();
      findFileInputs();
      getAllRemotedForms();
      getAllLists();
      getDatepickers();
      getAllMaskInputs();
      handleCheckboxInputSwapper();
      startStripeFramework();
      handleRestrictedModal();
      modalLogic();
    }
  });
}


function loadingEvent(receiver) {
  svgCircle = '<svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"><path opacity="0.2" fill="#000" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946 s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634 c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"/><path fill="#000" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0 C22.32,8.481,24.301,9.057,26.013,10.047z"><animateTransform attributeType="xml" attributeName="transform" type="rotate" from="0 20 20" to="360 20 20" dur="0.8s" repeatCount="indefinite"/></path></svg>'
  svgThreeDots = '<svg width="120" height="30" viewBox="0 0 120 30" xmlns="http://www.w3.org/2000/svg" fill="rgb(150,150,160)"><circle cx="15" cy="15" r="15"><animate attributeName="r" from="15" to="15" begin="0s" dur="0.8s" values="15;9;15" calcMode="linear" repeatCount="indefinite" /><animate attributeName="fill-opacity" from="1" to="1" begin="0s" dur="0.8s" values="1;.5;1" calcMode="linear" repeatCount="indefinite" /></circle><circle cx="60" cy="15" r="9" fill-opacity="0.3"><animate attributeName="r" from="9" to="9" begin="0s" dur="0.8s" values="9;15;9" calcMode="linear" repeatCount="indefinite" /><animate attributeName="fill-opacity" from="0.5" to="0.5" begin="0s" dur="0.8s" values=".5;1;.5" calcMode="linear" repeatCount="indefinite" /></circle><circle cx="105" cy="15" r="15"><animate attributeName="r" from="15" to="15" begin="0s" dur="0.8s" values="15;9;15" calcMode="linear" repeatCount="indefinite" /><animate attributeName="fill-opacity" from="1" to="1" begin="0s" dur="0.8s" values="1;.5;1" calcMode="linear" repeatCount="indefinite" /></circle></svg>'
  svgBis = '<svg width="40" height="40" viewBox="0 0 40 40" xmlns="http://www.w3.org/2000/svg" stroke="rgb(200,200,210)"><g fill="none" fill-rule="evenodd" stroke-width="2"><circle cx="22" cy="22" r="1"><animate attributeName="r" begin="0s" dur="1.8s" values="1; 20" calcMode="spline" keyTimes="0; 1" keySplines="0.165, 0.84, 0.44, 1" repeatCount="indefinite" /><animate attributeName="stroke-opacity" begin="0s" dur="1.8s" values="1; 0" calcMode="spline" keyTimes="0; 1" keySplines="0.3, 0.61, 0.355, 1" repeatCount="indefinite" /></circle><circle cx="22" cy="22" r="1"><animate attributeName="r" begin="-0.9s" dur="1.8s" values="1; 20" calcMode="spline" keyTimes="0; 1" keySplines="0.165, 0.84, 0.44, 1" repeatCount="indefinite" /><animate attributeName="stroke-opacity" begin="-0.9s" dur="1.8s" values="1; 0" calcMode="spline" keyTimes="0; 1" keySplines="0.3, 0.61, 0.355, 1" repeatCount="indefinite" /></circle></g></svg>';
  text = "<h5 class='ajax-loading-text genuine-font fs-9'>chargement</h5>";
  svg = '<?xml version="1.0" encoding="utf-8"?><svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 213 290.8" style="enable-background:new 0 0 213 290.8;" xml:space="preserve"><style type="text/css">.st0{fill:#00A2D1;}.st1{fill:#75260E;}.st2{fill:#5A1F0E;}.st3{fill:#070807;}.st4{fill:#90523E;}.st5{fill:#6F341F;}.st6{fill:#8F3A1F;}.st7{fill:#00143F;}</style><path class="st0" d="M196.1,145.3c0,49.5-40.4,89.6-90.2,89.6c-49.6,0-90.2-40.3-90.2-89.6c0-47.5,40.4-87.7,90.2-87.7 S196.1,97.9,196.1,145.3z M177.8,145.3c0-38-31.7-70-71.7-70s-71.7,32-71.7,70c0,40,31.7,72,71.7,72S177.8,185.3,177.8,145.3z"/><g><g><path class="st1" d="M31.9,92.7c0,0-1.7-31,0.8-48.1C36,23.3,46.3,0,106.6,0c64.2,0,71.4,23.2,73.7,44.6 c1.8,17.6,0.3,44.7,0.3,44.7s-32.4,16.2-73.7,16.2C62.8,105.6,31.9,92.7,31.9,92.7z"/></g><g><path class="st2" d="M87.9,0.8c0,0-27.1,14.5-21.1,46.1c5.9,31.6,43.9,41.8,67.5,45.6s34.8,0.3,34.8,0.3s-20.1,14-74.5,9.8 c0,0-47.4-2-62.9-14.8c0,0-3.1-39.3,5.6-59.4C37.4,28.4,43.3,5.3,87.9,0.8z"/></g><g><path class="st3" d="M31.5,77.2c0,0,36.6,18.7,72.4,18.7s77-18.6,77-18.6l-0.3,12c0,0-41.2,16.4-73.7,16.2s-66-11.9-75.1-17.9 L31.5,77.2z"/></g><g><path class="st4" d="M212.3,78.3c-3.9-15.9-23.5,2.8-30.1,7c-16.5,10.3-43.6,17.3-75.8,17.3c-35.8,0-59.6-6.9-75.9-17.3 c-6.3-4.1-24.6-22.7-29.4-6.9c-5.5,17.9,11.6,24.2,11.6,24.2s46.8,21.8,94.1,21.8c50.3,0,93.8-22,93.8-22S216.4,94.9,212.3,78.3z"/></g><g><path class="st5" d="M14.6,79c0,0,35,36.4,75.8,38.2c46.6,2.2,75.4-1.7,75.4-1.7s-33.7,9.8-71.3,8.9c-37.6-1-66.8-15.1-81.8-22 S0.3,87.3,1.1,78.2C2,69.2,14.6,79,14.6,79z"/></g><g><ellipse class="st6" cx="145.3" cy="28.5" rx="23.9" ry="13.3"/></g></g><g><path class="st7" d="M28.9,277.3c0,0.7-0.7,1-2,1H5c0.9,4.6,4.8,8.3,9.7,8.3c2.6,0,4.9-1.1,6.6-2.7c0.8-0.8,1.4-1.1,2.5-1.1h0.8 c1.6,0,2.4,1.1,1.4,2.4c-2.6,3.4-6.8,5.5-11.3,5.5c-7.8,0-14.3-6.7-14.3-14.4c0-7.8,6.4-14.3,14.3-14.3s14.3,6.4,14.3,14.3V277.3z M5,274.3h19.4c-0.9-4.6-4.8-8.1-9.7-8.1S5.9,269.7,5,274.3z"/><path class="st7" d="M60.3,290.1c-1.3,0-2-0.7-2-2v-2.4c-2.5,3.1-6.2,5.1-10.4,5.1c-7.8,0-14.3-6.7-14.3-14.4 c0-7.8,6.4-14.3,14.3-14.3c4.2,0,7.9,1.9,10.4,4.9v-19.7c0-1.3,0.7-2,2-2h0.2c1.3,0,2,0.7,2,2v40.7c0,1.3-0.7,2-2,2H60.3z M58.1,276.4c0-5.6-4.8-10.1-10.3-10.1c-5.6,0-9.8,4.6-9.8,10.1c0,5.4,4.2,10.3,9.8,10.3C53.2,286.6,58.1,281.8,58.1,276.4z"/><path class="st7" d="M112,274.4v13.7c0,1.3-0.7,2-2,2h-0.2c-1.3,0-2-0.7-2-2v-13.7c0-5.4-2.1-8.1-6.1-8.1c-5.1,0-8.3,2.8-8.3,9.5 v12.3c0,1.3-0.7,2-2,2h-0.2c-1.3,0-2-0.7-2-2v-13.7c0-5.4-2.1-8.1-6.1-8.1c-5.1,0-8.3,2.8-8.3,9.5v12.3c0,1.3-0.7,2-2,2h-0.2 c-1.3,0-2-0.7-2-2v-23.5c0-1.3,0.7-2,2-2h0.2c1.3,0,2,0.7,2,2v1.3c2-2.4,4.8-3.8,8.3-3.8c4.8,0,7.8,1.6,9.3,5.3 c1.9-3.4,5.2-5.3,9.3-5.3C108.8,262.1,112,265.6,112,274.4z"/><path class="st7" d="M118.3,276.4c0-7.8,6.4-14.3,14.3-14.3c7.8,0,14.3,6.4,14.3,14.3c0,7.7-6.4,14.4-14.3,14.4 C124.7,290.8,118.3,284.1,118.3,276.4z M122.7,276.4c0,5.4,4.3,10.3,9.9,10.3s9.9-4.8,9.9-10.3c0-5.6-4.3-10.1-9.9-10.1 S122.7,270.8,122.7,276.4z"/><path class="st7" d="M155.4,290.1c-1.3,0-2-0.7-2-2v-23.5c0-1.3,0.7-2,2-2h0.2c1.3,0,2,0.7,2,2v1.3c2.2-2.4,5.3-3.8,9-3.8 c7.9,0,11.3,3.5,11.3,12.3v13.7c0,1.3-0.7,2-2,2h-0.2c-1.3,0-2-0.7-2-2v-13.7c0-5.4-2.5-8.1-7.1-8.1c-5.6-0.1-9,3-9,9.2v12.6 c0,1.3-0.7,2-2,2H155.4z"/><path class="st7" d="M210.8,290.1c-1.3,0-2-0.7-2-2v-2.4c-2.5,3.1-6.2,5.1-10.4,5.1c-7.8,0-14.3-6.7-14.3-14.4 c0-7.8,6.4-14.3,14.3-14.3c4.2,0,7.9,1.9,10.4,4.9v-19.7c0-1.3,0.7-2,2-2h0.2c1.3,0,2,0.7,2,2v40.7c0,1.3-0.7,2-2,2H210.8z M208.5,276.4c0-5.6-4.8-10.1-10.3-10.1c-5.6,0-9.8,4.6-9.8,10.1c0,5.4,4.2,10.3,9.8,10.3C203.7,286.6,208.5,281.8,208.5,276.4z"/></g></svg>'
  element = "<section class='loading-section fadein'>" + svgCircle + text + "</div>";
  if (receiver) {
    receiver.innerHTML = element;
  }
}

document.addEventListener("DOMContentLoaded",function(){
  startDashboard();
  ajaxNavigation(getAllLinks());
  findAllCharts();
  getAllRemotedForms();
  getAllLists();
  getDatepickers();
  getAllMaskInputs();
});
