class AjaxResponder {
  constructor(){};
}

AjaxResponder.htmlResponse = function(data,receiver,url,trackable) {
  if (data.redirect) {
    console.log("Redirect key is present");
    receiver.innerHTML = data.html;
    setInterval(() => window.location.replace(data.redirect), 2500);
  } else {
    receiver.innerHTML = data;
    if (trackable == "true") {
      window.history.pushState(null, "Title", url);
    }
  }
}
AjaxResponder.jsonResponse = function(data,receiver,url,trackable) {
  console.log("AjaxResponder jsonResponse");
  console.log(receiver);
}
AjaxResponder.errorResponse = function(data) {
  console.log("AjaxResponder errorResponse");
  var errorUrl = document.head.querySelector("meta[name=error_url]").content;
  window.location = errorUrl;
}

