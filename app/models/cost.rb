class Cost
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  belongs_to :company
  # validates_presence_of :year, :rent, :wages, :others
  validates_presence_of :year, :month, :company_id
  validates_uniqueness_of :month, scope: [:company_id,:year]

  before_save :format_mask_integer
  before_save :handle_empty_values

  field :month, type: Integer
  field :year, type: Integer
  field :rent, type: String, default: "0"
  field :subscriptions, type: String, default: "0"
  field :wages, type: String, default: "0"
  field :others, type: String, default: "0"
  field :int_values, type: Hash, default: {rent: 0,subscriptions: 0,wages: 0,others: 0}
  field :year_months_reference, type: Boolean, default: nil

  def total
    [self.rent.to_i,self.wages.to_i,self.subscriptions.to_i,self.others.to_i].sum
  end

  def datas
    [self.rent.to_i,self.wages.to_i,self.subscriptions.to_i,self.others.to_i]
  end


  private

  def handle_empty_values
    if rent.to_s.empty?
      puts "---- RENT PARAMS ----"
      puts rent
      self[:rent] = "0"
    end
    if subscriptions.to_s.empty?
      puts "---- SUBSCRIPTIONS PARAMS ----"
      puts subscriptions
      self[:subscriptions] = "0"
    end
    if wages.to_s.empty?
      puts "---- WAGES PARAMS ----"
      puts wages
      self[:wages] = "0"
    end
    if others.to_s.empty?
      puts "---- OTHERS PARAMS ----"
      puts others
      self[:others] = "0"
    end
  end

  def format_mask_integer
    puts "---- Inside format mask integer ----"
    if self[:rent]
      self[:rent] = self[:rent].to_s.gsub(/[[:space:]]/,'')
      self[:int_values][:rent] = self[:rent].to_s.gsub(/[[:space:]]/,'').to_i
    end

    if self[:subscriptions]
      self[:subscriptions] = self[:subscriptions].to_s.gsub(/[[:space:]]/,'')
      self[:int_values][:subscriptions] = self[:subscriptions].to_s.gsub(/[[:space:]]/,'').to_i
    end

    if self[:wages]
      self[:wages] = self[:wages].to_s.gsub(/[[:space:]]/,'')
      self[:int_values][:wages] = self[:wages].to_s.gsub(/[[:space:]]/,'').to_i
    end

    if self[:others]
      self[:others] = self[:others].to_s.gsub(/[[:space:]]/,'')
      self[:int_values][:others] = self[:others].to_s.gsub(/[[:space:]]/,'').to_i
    end
    puts "------------------------------------"
  end

end
