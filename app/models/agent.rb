class Agent
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  belongs_to :company

  field :firstname, type: String
  field :lastname, type: String

end
