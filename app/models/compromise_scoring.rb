class CompromiseScoring
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  belongs_to :compromise

  field :sale_type, type: Integer
  field :seller_type, type: Integer
  field :writing_quality, type: Integer
  field :weak_signals, type: Integer
  field :seller_risk, type: Integer
  field :buyer_risk, type: Integer

  def sale_type
    if self[:sale_type]
      values = {0 => "Ancien", 1 => "Neuf", 2 => "Viager"}
      value = values[self[:sale_type]]
    else
      value = nil
    end
    return value
  end

  def seller_type
    if self[:seller_type]
      values = {0 => "Particulier", 1 => "Société", 2 => "Succession"}
      value = values[self[:seller_type]]
    else
      value = nil
    end
    return value
  end

  def writing_quality
    if self[:writing_quality]
      values = {0 => "1", 1 => "2", 2 => "3", 3 => "4", 4 => "5"}
      value = values[self[:writing_quality]]
    else
      value = nil
    end
    return value
  end

  def weak_signals
    if self[:weak_signals]
      values = {0 => "1", 1 => "2", 2 => "3", 3 => "4", 4 => "5"}
      value = values[self[:weak_signals]]
    else
      value = nil
    end
    return value
  end

  def seller_risk
    if self[:seller_risk]
      values = {0 => "Fort", 1 => "Assez fort", 2 => "Moyen", 3 => "Assez faible", 4 => "Très faible"}
      value = values[self[:seller_risk]]
    else
      value = nil
    end
    return value
  end

  def buyer_risk
    if self[:buyer_risk]
      values = {0 => "Fort", 1 => "Assez fort", 2 => "Moyen", 3 => "Assez faible", 4 => "Très faible"}
      value = values[self[:buyer_risk]]
    else
      value = nil
    end
    return value
  end

end
