class Subscription
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  belongs_to :company

  validates_presence_of :name,:category,:amount,:start_date,:recurrence
  validates_uniqueness_of :name, scope: :company_id
  validate :consistent_dates

  before_save :format_mask_integer
  before_save :set_end_date

  scope :monthly_subs, -> {where(recurrence: 1)}
  scope :annual_subs, -> {where(recurrence: 2)}
  scope :ninty_unnotified, -> {where(ninty_mail_notified: false)}
  scope :sixty_unnotified, -> {where(sixty_mail_notified: false)}
  scope :thirty_unnotified, -> {where(thirty_mail_notified: false)}

  scope :company_subs_end, lambda { |params| where(:company_id => params[:company_id], end_date: {"$gt": Date.today + params[:start].to_i.send("days") ,"$lte": Date.today + params[:end].to_i.send("days")}) }

  field :name, type: String
  field :slug_name, type: String
  field :category, type: Integer
  field :amount, type: String
  field :int_amount, type: Integer
  field :start_date, type: Date
  field :end_date, type: Date
  field :recurrence, type: Integer
  field :subitem_id, type: String

  field :closed, type: Boolean
  field :renewed, type: Boolean

  field :ninty_mail_notified, type: Boolean, default: false
  field :sixty_mail_notified, type: Boolean, default: false
  field :thirty_mail_notified, type: Boolean, default: false

  def year_sum
    size = (self.start_date..self.end_date).to_a.map{|date| [date.month,date.year]}.uniq.size
    if self[:recurrence] == 1
      (self.int_amount * size.to_i)
    else
      self.int_amount
    end
  end

  def annual?
    self[:recurrence] == 2 ? true : false
  end

  def recurrence
    values = {1 => "Mensuelle", 2 => "Annuelle"}
    if self[:recurrence]
      result = values[self[:recurrence]]
    else
      result = nil
    end
  end

  def category_icon
    values = {1 => "megaphone", 2 => "postcard", 3 => "window", 4 => "share-alt", 5 => "star", 6 => "umbrella", 7 => "unicon-phone", 8 => "store-alt"}
    if self[:category]
      result = values[self[:category]]
    else
      result = nil
    end
    return result
  end

  def category
    values = {1 => "Portails - diffusion", 2 => "Vitrine", 3 => "Logiciels", 4 => "Réseau", 5 => "Franchise", 6 => "Garantie - Assurances", 7 => "Télécom", 8 => "Locaux - véhicules"}
    if self[:category]
      result = values[self[:category]]
    else
      result = nil
    end
  end

  def duration
    if self.start_date && self.end_date
      puts "---- Sub duration ----"
      result = self.end_date - self.start_date
      start_date = "#{self.start_date.year}-#{self.start_date.month}"
      end_date = "#{self.end_date.year}-#{self.end_date.month}"
      puts (self.start_date..self.end_date).step(1.month).to_a
      puts result.to_i.inspect
      puts "----------------------"
      return result.to_i
    end
  end

  private

  def format_mask_integer
    if self[:amount]
      self[:amount] = self[:amount].to_s.gsub(/[[:space:]]/,'')
      self[:int_amount] = self[:amount].to_s.gsub(/[[:space:]]/,'').to_i
    end
  end

  def set_end_date
    puts "---- inside set_end_date method -----"
    puts end_date.inspect
    if !self[:end_date]
      puts "---- EMPTY END DATE ----"
      self[:end_date] = Date.today.end_of_year
      puts "€€€€ #{self[:end_date]} €€€€"
    end
  end

  def consistent_dates
    if self[:start_date] && self[:end_date]
      puts "---- Dates present ----"
      if self[:start_date].to_date > self[:end_date].to_date
        errors.add(:start_date, :inconsistent)
      end
    end
  end

end
