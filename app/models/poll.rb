class Poll
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  validates_uniqueness_of :planification

  field :planification, type: Integer
  field :question, type: String
  field :responses, type: Hash
  field :negative, type: Integer, default: 0
  field :mitigate, type: Integer, default: 0
  field :positive, type: Integer, default: 0

  def percent_calculate
    total = self.negative + self.mitigate + self.positive
    calculated_negative = (self.negative.to_f / total.to_f) * 100
    calculated_mitigate = (self.mitigate.to_f / total.to_f) * 100
    calculated_positive = (self.positive.to_f / total.to_f) * 100
    result = {sad: calculated_negative.to_i, mitigate: calculated_mitigate.to_i, happy: calculated_positive.to_i}
  end

  def total
    self.negative + self.mitigate + self.positive
  end

end
