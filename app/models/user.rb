class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  has_many :companies, autosave: true
  has_one :user_scoring
  has_and_belongs_to_many :polls, inverse_of: nil

  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable
  accepts_nested_attributes_for :companies


  validates :cgu, acceptance: { accept: [true,1,"true","1"] }
  validates_presence_of :is_legal_agent

  # Devise field
  field :email, type: String, default: ""
  field :encrypted_password, type: String, default: ""
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time
  field :remember_created_at, type: Time
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  # Classic fields
  field :firstname, type: String
  field :lastname, type: String
  field :business_card, type: String
  field :personal_email, type: String
  field :phone, type: String
  field :points, type: Integer, default: 0
  field :is_legal_agent, type: Integer, default: nil

  # Stripe fields
  field :stripe_customer_id, type: String, default: nil
  field :stripe_coupon_id, type: String, default: nil
  field :stripe_coupon_name, type: String, default: nil
  field :stripe_coupon_percent_off, type: Integer, default: nil
  field :stripe_coupon_amount_off, type: Float, default: nil
  field :payment_method_id, type: String, default: nil
  field :stripe_subscription_id, type: String, default: nil
  field :active_stripe_subscription, type: Boolean, default: false
  field :stripe_subscription_status, type: String, default: nil
  field :stripe_product_id, type: String, default: nil
  field :stripe_product_name, type: String, default: nil
  field :stripe_subscription_start_date, type: Date, default: nil
  field :stripe_card_datas, type: Hash, default: nil
  field :stripe_subscription_plan_amount, type: String, default: nil
  field :stripe_trial_period_days, type: Integer, default: nil
  field :stripe_trial_end, type: Time, default: nil
  field :stripe_plan_interval, type: String, default: nil
  field :stripe_plan_amount, type: Float, default: nil

  # Classic url image field
  field :id_card_remote, type: String

  # RGPD Cgu
  field :cgu, type: Boolean

  include Wizard

  def display_plan_amount
    if self.stripe_plan_interval && self.stripe_plan_amount
      if self.stripe_coupon_percent_off
        calculation = (self.stripe_plan_amount - (self.stripe_plan_amount * (self.stripe_coupon_percent_off.to_f / 100))).round(2)
        value = "#{calculation} € / #{self.stripe_plan_interval}"
      elsif self.stripe_coupon_amount_off
        value = "#{self.stripe_plan_amount.to_f - self.stripe_coupon_amount_off.to_f} € / #{self.stripe_plan_interval}"
      else
        value = "#{self.stripe_plan_amount} € / #{self.stripe_plan_interval}"
      end
    else
      value = nil
    end
    return value
  end

  def customer_sub_coupon
    if self.stripe_subscription_id
      value = Stripe::Subscription.retrieve(self.stripe_subscription_id)
    else
      value = nil
    end
    value ? value.as_json : nil
  end

  def sub_trial_period_time
    if self.stripe_trial_end
      return self.stripe_trial_end
    else
      trial_end = self.retrieve_stripe_account.as_json["subscriptions"]["data"].first["trial_end"]
      if trial_end != nil
        ruby_date = Time.at(trial_end).to_datetime
      else
        ruby_date = nil
      end
      if ruby_date
        self.stripe_trial_end = ruby_date
        self.save(validate: false)
      end
      return ruby_date
    end
  end

  def stripe_trial_status
    if self.stripe_subscription_status
      if self.stripe_subscription_status == "trialing"
        value = "Période d'essai"
      elsif self.stripe_subscription_status == "canceled"
        value = "Annulé"
      else
        value = ""
      end
    else
      value = nil
    end
    return value
  end

  def remove_all_stripe_reference
    self.stripe_customer_id = nil
    self.stripe_subscription_id = nil
    self.stripe_product_id = nil
    self.stripe_product_name = nil
    self.stripe_subscription_status = nil
    self.stripe_subscription_start_date = nil
    self.stripe_card_datas = nil
    self.payment_method_id = nil
    self.active_stripe_subscription = false
    self.stripe_subscription_plan_amount = nil
    self.stripe_trial_period_days = nil
    self.stripe_trial_end = nil
    self.stripe_coupon_id = nil
    self.stripe_coupon_name = nil
    self.stripe_coupon_percent_off = nil
    self.stripe_coupon_amount_off = nil
    self.stripe_plan_interval = nil
    self.stripe_plan_amount = nil

    if self.save
      puts "Successfully remove all stripe reference"
    else
      puts "An error occured when trying to remove stripe reference"
    end
  end

  def stripe_invoices
    data = Stripe::Invoice.list(customer: self.stripe_customer_id)
    puts data.inspect
    return data
  end

  def is_stripe_customer?
    self[:stripe_customer_id] ? true : false
  end

  def trial_used?
    if self[:stripe_subscription_status]
      if self[:stripe_subscription_status] == "canceled"
        used_trial = true
      else
        used_trial = false
      end
    else
      used_trial = false
    end
    return used_trial
  end

  def has_platform_access?
    if self[:stripe_customer_id]
      is_customer = true
      if self[:stripe_subscription_status]
        if self[:stripe_subscription_status] == "trialing"
          active_sub = true
        elsif self[:stripe_subscription_status] == "active"
          active_sub = true
        else
          active_sub = false
        end
      else
        active_sub = false
      end
    else
      is_customer = false
    end
    return is_customer && active_sub
  end

  def retrieve_stripe_account
    customer = self.is_stripe_customer? ? Stripe::Customer.retrieve(self.stripe_customer_id) : nil
    return customer
  end

  def upcoming_invoice
    customer = self.is_stripe_customer? ? Stripe::Invoice.upcoming(customer: self.stripe_customer_id) : nil
    if customer
      value = Time.at(customer.as_json["created"]).to_date
    else
      value = nil
    end
    return value
  end

  def stripe_subscription
    subscription = self.stripe_subscription_id ? Stripe::Subscription.retrieve(self.stripe_subscription_id) : nil
  end

  def stripe_product
    self.stripe_product_id ? Stripe::Product.retrieve(self.stripe_product_id) : nil
  end

  def last_subscription_invoice
    subscription = self.stripe_subscription
    @invoice = subscription ? Stripe::Invoice.retrieve(subscription["latest_invoice"]) : nil
  end

  def stripe_card_infos
    if self.is_stripe_customer?
      payments = Stripe::PaymentMethod.list({customer: self.stripe_customer_id, type: 'card',})
      card = payments["data"].first
      card_infos = {brand: card["card"]["brand"], country: card["card"]["country"], exp_month: card["card"]["exp_month"], exp_year: card["card"]["exp_year"], last4: card["card"]["last4"]}
      puts card_infos.inspect
    else
      card_infos = nil
    end
    return card_infos
  end

end
