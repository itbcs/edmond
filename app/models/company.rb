class Company
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  belongs_to :user, optional: true
  has_many :compromises
  has_many :shareholders, autosave: true
  has_many :agents
  has_many :costs
  has_many :subscriptions
  has_many :financing_requests
  accepts_nested_attributes_for :shareholders

  validates_presence_of :trade_name, :if => lambda { |o| o.user.current_step == "two" }
  validates_presence_of :name, :if => lambda { |o| o.user.current_step == "two" }
  validates_presence_of :company_type, :if => lambda { |o| o.user.current_step == "two" }

  validate :correct_gouv_api_search

  field :name, type: String
  field :siren, type: String
  field :siret, type: String
  field :legal_name, type: String
  field :trade_name, type: String
  field :company_type, type: Integer
  field :balance_closing_date, type: Time
  field :city, type: String
  field :postal_code, type: String
  field :juridical_nature, type: String
  field :activity_start_date, type: Date
  field :full_address, type: String
  field :company_api_search, type: String

  # Classic url image field
  field :kbis_remote, type: String

  def compromises_contract_types
    if self.compromises?
      values = {}
      promises = self.compromises.where(contract_type: 1).size
      compromises = self.compromises.where(contract_type: 0).size
      values[:compromises] = compromises
      values[:promises] = promises
    else
      values = nil
    end
    return values
  end

  def compromises_funding
    if self.compromises?
      values = {}
      positives = self.compromises.where(pending_funding_terms: 0).size
      negatives = self.compromises.where(pending_funding_terms: 1).size
      values[:positives] = positives
      values[:negatives] = negatives
    else
      values = nil
    end
    return values
  end

  def compromises_object
    if self.compromises
      values = {}
      houses = self.compromises.where(purchase_type: 0).size
      flats = self.compromises.where(purchase_type: 1).size
      terrains = self.compromises.where(purchase_type: 2).size
      values[:houses] = houses
      values[:flats] = flats
      values[:terrains] = terrains
    else
      values = nil
    end
    return values
  end

  def notary_signature
    if self.compromises?
      values = {}
      notary_place = self.compromises.where(compromise_signature_place: 1).size
      if notary_place != 0
        notary_percent = ((notary_place.to_f / self.compromises.size.to_f) * 100).round(0).to_i
      else
        notary_percent = 0
      end


      agency_place = self.compromises.where(compromise_signature_place: 0).size
      if agency_place != 0
        agency_percent = ((agency_place.to_f / self.compromises.size.to_f) * 100).round(0).to_i
      else
        agency_percent = 0
      end

      values[:collection] = self.compromises.size
      values[:notary_size] = notary_place
      values[:notary_percent] = notary_percent
      values[:agency_size] = agency_place
      values[:agency_percent] = agency_percent

      puts "€€€€€€€"
      puts "---- NOTARY SIGNATURE ----"
      puts self.compromises.size
      puts "€€€€€€€"
    else
      values = nil
    end
    return values
  end


  def correct_gouv_api_search
    if company_api_search
      if !company_api_search.include?("-")
        errors.add(:company_api_search, :uncorrect)
      end
    end
  end

  def juridical_nature
    codes = {1000 => "Auto entrepreneur", 5498 => "EURL", 5499 => "SARL", 5710 => "SAS", 5720 => "SA"}
    result = self[:juridical_nature] ? codes[self[:juridical_nature].to_i] : nil
  end

  def company_type
    values = {0 => "SARL", 1 => "SA", 2 => "EURL", 3 => "SAS", 4 => "Auto-entrepreneur", 5 => "Autre"}
    if self[:company_type]
      result = values[self[:company_type]]
    else
      result = nil
    end
  end

  def monthly_subs
    self.subscriptions.where(recurrence: 1)
  end

  def annual_subs
    self.subscriptions.where(recurrence: 2)
  end

  def subscriptions_by_category
    results = {}
    if self.subscriptions?
      self.subscriptions.each do |sub|
        if results.has_key?(sub.category)
          results[sub.category] << sub.slug_name
        else
          results[sub.category] = [sub.slug_name]
        end
      end
    else
      result = nil
    end
    return results
  end

  def subscriptions_names
    if self.subscriptions?
      result = self.subscriptions.map{|sub| sub.slug_name}
    else
      result = []
    end
  end

  def subscriptions_amount_sum
    if self.subscriptions?
      result = self.subscriptions.map{|sub| sub.int_amount}.sum
    else
      result = nil
    end
    result
  end

  def signed_compromises_fees_sum
    if self.compromises?
      signed = self.compromises.where(status: 0).last
      if signed
        result = self.compromises.where(status: 0).map{|item| item.df_fees.to_i}.sum
      else
        result = nil
      end
    else
      result = nil
    end
    result
  end

  def unsigned_compromises_fees_sum
    if self.compromises?
      signed = self.compromises.where(status: {"$ne" => 0}).last
      if signed
        result = self.compromises.where(status: {"$ne" => 0}).map{|item| item.df_fees.to_i}.sum
      else
        result = nil
      end
    else
      result = nil
    end
    result
  end

  def cost_reference
    if self.costs?
      reference = self.costs.where(year_months_reference: true).last
      if reference
        result = reference
      else
        result = false
      end
    else
      result = false
    end
    result
  end

end
