class FinancingRequest
  include Mongoid::Document
  include ActiveModel::Validations
  include Mongoid::Timestamps
  include GlobalID::Identification

  belongs_to :company

  validates_presence_of :company_creation_year,:year_transactions_count,:pending_transactions_count,:annual_revenue,:total_transactions_amount
  before_save :format_mask_integer

  field :year_transactions_count, type: String
  field :pending_transactions_count, type: String
  field :annual_revenue, type: String
  field :total_transactions_amount, type: String
  field :company_creation_year, type: Integer
  field :status, type: Integer


  def status
    if self[:status]
      values = {0 => "En attente", 1 => "Traitée", 2 => "Annulée"}
      value = values[self[:status]]
    else
      value = nil
    end
    return value
  end

  def format_mask_integer
    puts "---- Inside format mask integer ----"
    puts self.inspect
    if self[:year_transactions_count]
      puts year_transactions_count.to_s.gsub(/[[:space:]]/,'').to_i
    end
    if pending_transactions_count
      puts pending_transactions_count.to_s.gsub(/[[:space:]]/,'').to_i
    end
    if annual_revenue
      puts "---- Inside annual revenue ----"
      puts self[:annual_revenue]
      puts "-------------------------------"
      self[:annual_revenue] = self[:annual_revenue].to_s.gsub(/[[:space:]]/,'').to_i
    end
    if total_transactions_amount
      self[:total_transactions_amount] = self[:total_transactions_amount].to_s.gsub(/[[:space:]]/,'').to_i
    end
    puts "------------------------------------"
  end

end
