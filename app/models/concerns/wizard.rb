module Wizard
  extend ActiveSupport::Concern

  included do
    attr_writer :current_step

    validates_presence_of :lastname, :if => lambda { |o| o.current_step == "one" }
    validates_presence_of :firstname, :if => lambda { |o| o.current_step == "one" }
    validates_presence_of :phone, :if => lambda { |o| o.current_step == "one" }

    def current_step
      @current_step || steps.first
    end

    def steps
      %w[one]
    end

    def next_step
      self.current_step = steps[steps.index(current_step)+1]
    end

    def previous_step
      self.current_step = steps[steps.index(current_step)-1]
    end

    def first_step?
      current_step == steps.first
    end

    def last_step?
      current_step == steps.last
    end

    def all_valid?
      steps.all? do |step|
        self.current_step = step
        valid?
      end
    end

  end

end
