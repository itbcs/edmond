class SubscriptionType
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  has_many :subscription_items

  field :name, type: String
  field :slug_name, type: String
  field :required, type: Boolean
  field :input_value, type: Integer
  field :svg_name, type: String


  def self.radiobuttons_lists_with_icons
    values = SubscriptionType.all.map{|sub_type| [sub_type.input_value, sub_type.checkbox_with_icon(label: sub_type.name, svg_name: sub_type.svg_name)]}
    return values
  end

  def checkbox_with_icon(args={})
    label = args[:label] ? args[:label] : false
    svg_name = args[:svg_name] ? args[:svg_name] : false
    icon = self.unicon_svg(name: svg_name, size: 30, color: "rgb(80,90,110)")
    if label.length > 14
      return ("<div class='d-flex d-column ai-center jc-between height100'><div class='svg-container'>#{icon}</div><div class='input-with-icon-label smaller'>#{label}</div></div>").html_safe
    else
      return ("<div class='d-flex d-column ai-center jc-between height100'><div class='svg-container'>#{icon}</div><div class='input-with-icon-label'>#{label}</div></div>").html_safe
    end
  end

  def unicon_svg(args={})
    return nil if !args[:name]
    name = args[:name]
    file_path = "#{Rails.root}/app/assets/images/svg/unicon/#{name}.svg"
    file = File.read(file_path).html_safe if File.exists?(file_path)
    xml = Nokogiri::XML(file)
    svg = xml.at_css("svg")
    size = args[:size].to_s
    color = args[:color]

    if color
      all_path = xml.at_css("path")
      all_path.set_attribute("fill",color)
    end

    if size
      svg.attributes["width"].value = size
      svg.attributes["height"].value = size
      return svg.to_html.html_safe
    else
      return File.read(file_path).html_safe if File.exists?(file_path)
      '(not found)'
    end

  end

end
