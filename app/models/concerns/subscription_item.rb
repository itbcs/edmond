class SubscriptionItem
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  belongs_to :subscription_type

  field :name, type: String
  field :slug_name, type: String
  field :svg_name, type: String

  def self.slug_names
    self.all.map{|item| item.slug_name}
  end

end
