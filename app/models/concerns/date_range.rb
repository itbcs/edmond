class DateRange
  attr_accessor :start_date, :end_date

  def self.current_full_year
    start_month = Date.today.beginning_of_year
    end_month = Date.today.end_of_year
    current_year = end_month.year.to_i
    DateRange.new(start_month,end_month)
  end

  def initialize(start_date,end_date)
    @start_date = start_date
    @end_date = end_date
  end

  def french_dates
    ["empty","Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre"]
  end

  def abbr_french_dates
    ["empty","Jan.","Fév.","Mars","Avr.","Mai","Juin","Jui.","Aoû.","Sep.","Oct.","Nov.","Déc."]
  end

  def int_french_dates
    [0,1,2,3,4,5,6,7,8,9,10,11,12]
  end

  def range_list
    range = (self.start_date..self.end_date).to_a
    return range.map{|item| item.to_s[0...-3]}.uniq.map{|item| abbr_french_dates[item.to_s[5..-1].to_i]}
  end

  def integer_list
    range = (self.start_date..self.end_date).to_a
    return range.map{|item| item.to_s[0...-3]}.uniq.map{|item| int_french_dates[item.to_s[5..-1].to_i]}
  end

end
