class UserQuestion
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  validates_presence_of :object, :message

  field :user_id, type: String
  field :object, type: String
  field :message, type: String

end
