class UserScoring
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  belongs_to :user

  field :funds, type: Integer
  field :incident, type: Integer
  field :min_balance_sheets, type: Integer
  field :n_minus_one_result, type: Integer
  field :n_minus_two_result, type: Integer
  field :treasury, type: Integer
  field :treasury_incident, type: Integer
  field :activity_turnover_evolution, type: Integer
  field :activity_transaction_evolution, type: Integer
  field :already_customer, type: Integer
  field :have_website, type: Integer
  field :sign, type: Integer
  field :active_on_social_networks, type: Integer
  field :one_year_market_trend, type: Integer
  field :market_confidence_index, type: Integer
  field :compromises_failure_rate, type: Integer


  def valid_scoring?
    items = []
    attributes_black_list = ["_id", "user_id", "updated_at", "created_at"]
    self.attributes.each do |attribute|
      instance_attribute = attribute.first
      if attributes_black_list.include?(instance_attribute)
        puts "Black list item"
      else
        items << instance_attribute
      end
    end
    if items.size > 0
      return true
    else
      return false
    end
  end

  def funds
    if self[:funds]
      values = {1 => "< 0 €", 2 => "> 0 €", 3 => "> 25% du bilan"}
      value = values[self[:funds]]
    else
      value = nil
    end
    return value
  end

  def incident
    if self[:incident]
      values = {0 => "5 dernières années", 1 => "Entre 5 et 10 dernières années", 2 => "+ de 10 ans", 3 => "Pas d'incident"}
      value = values[self[:incident]]
    else
      value = nil
    end
    return value
  end

  def min_balance_sheets
    if self[:min_balance_sheets]
    else
      value = nil
    end
    return value
  end

  def n_minus_one_result
    if self[:n_minus_one_result]
      values = {0 => "< 0 €", 1 => "> 0 €", 2 => "> 5% du CA"}
      value = values[self[:n_minus_one_result]]
    else
      value = nil
    end
    return value
  end

  def n_minus_two_result
    if self[:n_minus_two_result]
      values = {0 => "< 0 €", 1 => "> 0 €", 2 => "> 5% du CA"}
      value = values[self[:n_minus_two_result]]
    else
      value = nil
    end
    return value
  end

  def treasury
    if self[:treasury]
      values = {0 => "- 3 mois", 1 => "+ 3 mois"}
      value = values[self[:treasury]]
    else
      value = nil
    end
    return value
  end

  def treasury_incident
    if self[:treasury_incident]
      values = {0 => "Incident < 0", 1 => "Incident > 0"}
      value = values[self[:treasury_incident]]
    else
      value = nil
    end
    return value
  end

  def activity_turnover_evolution
    if self[:activity_turnover_evolution]
      values = {0 => "Repli + 5%", 1 => "Repli - 5%", 2 => "ISO", 3 => "Prog + 5%", 4 => "Prog - 5%"}
      value = values[self[:activity_turnover_evolution]]
    else
      value = nil
    end
    return value
  end

  def activity_transaction_evolution
    if self[:activity_transaction_evolution]
      values = {0 => "Repli + 5%", 1 => "Repli - 5%", 2 => "ISO", 3 => "Prog + 5%", 4 => "Prog - 5%"}
      value = values[self[:activity_transaction_evolution]]
    else
      value = nil
    end
    return value
  end

  def already_customer
    if self[:already_customer]
      values = {0 => "Oui", 1 => "Non"}
      value = values[self[:already_customer]]
    else
      value = nil
    end
    return value
  end

  def have_website
    if self[:have_website]
      values = {0 => "Oui", 1 => "Non"}
      value = values[self[:have_website]]
    else
      value = nil
    end
    return value
  end

  def sign
    if self[:sign]
      values = {0 => "Franchisé", 1 => "Adhérent", 2 => "Mandataire", 3 => "Non"}
      value = values[self[:sign]]
    else
      value = nil
    end
    return value
  end

  def active_on_social_networks
    if self[:active_on_social_networks]
      values = {0 => "Peu d'activité", 1 => "Peu actif", 2 => "Actif", 3 => "Très actif"}
      value = values[self[:active_on_social_networks]]
    else
      value = nil
    end
    return value
  end

  def one_year_market_trend
    if self[:one_year_market_trend]
      values = {0 => "Excellente", 1 => "Bonne", 2 => "Moyenne", 3 => "Mauvaise", 4 => "Très mauvaise"}
      value = values[self[:one_year_market_trend]]
    else
      value = nil
    end
    return value
  end

  def market_confidence_index
    if self[:market_confidence_index]
      values = {0 => "1", 1 => "2", 2 => "3", 3 => "4", 4 => "5"}
      value = values[self[:market_confidence_index]]
    else
      value = nil
    end
    return value
  end

  def compromises_failure_rate
    if self[:compromises_failure_rate]
    else
      value = nil
    end
    return value
  end


end
