class Shareholder
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  belongs_to :company, optional: true

  field :name, type: String
end
