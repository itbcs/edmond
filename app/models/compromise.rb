class Compromise
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification
  attr_writer :pending_compromise

  belongs_to :company
  has_one :compromise_scoring

  validates_presence_of :reference, :fees, :seller_lastname, :compromise_signature_date, :selling_price, :authentic_act_signature_date
  validates_presence_of :purchase_type, if: :pending_compromise?
  validates_presence_of :contract_type, if: :pending_compromise?
  validates_presence_of :pending_funding_terms, if: :pending_compromise?
  validates_presence_of :status, if: :pending_compromise?
  validates_presence_of :compromise_signature_place, if: :pending_compromise?
  validates_uniqueness_of :reference, scope: :company_id
  validate :consistent_status, if: :pending_compromise?
  validate :consistent_dates, if: :unpending_compromise?
  validate :pending_consistent_dates, if: :pending_compromise?

  # before_save :set_authentic_act_status
  before_save :add_month_and_year_from_signature_date
  before_save :add_month_and_year_from_authentic_act_date
  before_save :format_mask_amount

  field :status, type: Integer, default: 1
  field :reference, type: String
  field :fees, type: String
  field :selling_price, type: String
  field :purchase_type, type: Integer
  field :seller_lastname, type: String
  field :contract_type, type: Integer
  field :pending_funding_terms, type: Integer
  field :compromise_signature_place, type: Integer
  field :compromise_signature_date, type: Date
  field :compromise_signature_date_month, type: Integer
  field :compromise_signature_date_year, type: Integer
  field :authentic_act_signature_date, type: Date
  field :authentic_act_signature_date_month, type: Integer
  field :authentic_act_signature_date_year, type: Integer
  field :df_fees, type: Integer
  field :df_selling_price, type: Integer

  def pending_compromise
    @pending_compromise
  end

  def pending_compromise?
    pending_compromise ? true : false
  end

  def unpending_compromise?
    pending_compromise ? false : true
  end

  def pending_funding_terms
    values = {0 => "oui", 1 => "non"}
    if self[:pending_funding_terms]
      result = values[self[:pending_funding_terms].to_i]
    else
      result = nil
    end
    return result
  end

  def compromise_signature_place
    values = {0 => "agence", 1 => "notaire"}
    if self[:compromise_signature_place]
      result = values[self[:compromise_signature_place].to_i]
    else
      result = nil
    end
    return result
  end

  def contract_type
    values = {0 => "Compromis", 1 => "Promesse de vente"}
    if self[:contract_type]
      result = values[self[:contract_type].to_i]
    else
      result = nil
    end
    return result
  end

  def status
    values = {0 => "Signé", 1 => "En attente", 2 => "Annulé", 3 => "Encaissé"}
    if self[:status]
      result = values[self[:status].to_i]
    else
      result = nil
    end
    return result
  end

  def purchase_type
    values = {0 => "maison", 1 => "appartement", 2 => "terrain"}
    if self[:purchase_type]
      result = values[self[:purchase_type].to_i]
    else
      result = nil
    end
    return result
  end

  private

  def format_mask_amount
    puts "---- Inside compromise before_save action => remove space from string ----"
    if self[:fees]
      self[:fees] = self[:fees].to_s.gsub(/[[:space:]]/,'')
      self[:df_fees] = duty_free_calculation(self[:fees])
    end
    if self[:selling_price]
      self[:selling_price] = self[:selling_price].to_s.gsub(/[[:space:]]/,'')
      self[:df_selling_price] = duty_free_calculation(self[:selling_price])
    end
  end

  def duty_free_calculation(price)
    result = price.to_f / 1.20
    result.round(0).to_i
  end

  def consistent_status
    if compromise_signature_date && status
      if status == "Signé" && Date.today < compromise_signature_date.to_date
        errors.add(:status, :inconsistent)
      end
    end
  end

  def consistent_dates
    if authentic_act_signature_date && compromise_signature_date
      if compromise_signature_date > authentic_act_signature_date
        errors.add(:compromise_signature_date, :inconsistent)
      elsif (compromise_signature_date > authentic_act_signature_date) && (authentic_act_signature_date > Date.today)
        errors.add(:compromise_signature_date, :inconsistent)
        errors.add(:authentic_act_signature_date, :after_today)
      elsif authentic_act_signature_date > Date.today
        errors.add(:authentic_act_signature_date, :after_today)
      end
    end
  end

  def pending_consistent_dates
    if authentic_act_signature_date && compromise_signature_date
      if compromise_signature_date > authentic_act_signature_date
        errors.add(:compromise_signature_date, :inconsistent)
      elsif authentic_act_signature_date < Date.today
        errors.add(:authentic_act_signature_date, :before_today)
      end
    end
  end

  def set_authentic_act_status
    if self[:authentic_act_signature_date]
      today = Date.today
      signature_date = self[:authentic_act_signature_date]
      if signature_date <= today
        self[:status] = 0
      end
    end
  end

  def add_month_and_year_from_signature_date
    if self[:compromise_signature_date]
      self[:compromise_signature_date_month] = self[:compromise_signature_date].month.to_i
      self[:compromise_signature_date_year] = self[:compromise_signature_date].year.to_i
    end
  end

  def add_month_and_year_from_authentic_act_date
    if self[:authentic_act_signature_date]
      self[:authentic_act_signature_date_month] = self[:authentic_act_signature_date].month.to_i
      self[:authentic_act_signature_date_year] = self[:authentic_act_signature_date].year.to_i
    end
  end

end
