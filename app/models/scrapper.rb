class Scrapper
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification
  include ApplicationHelper
  require "csv"

  field :url, type: String
  field :ssl, type: Boolean
  field :form_search_id, type: String
  field :form_search_field_name, type: String
  field :form_select_options_size, type: Integer
  field :item_link_scrapping_name, type: String
  field :doc_raw_dom_id, type: String

  FIELDS = ["Carte professionnelle n° ","Valable jusqu'au  :","Délivrée par :","le  :"," Le Président  \r\n\t :\r\n\t","Dénomination :","Forme juridique :","Adresse du siège :","Nom commercial :","N° unique d'identification  :"]

  def create_csv_file
    headers = ["Region","Carte professionnelle","Valable jusqu'au","Délivrée par","Délivrée Le","Le président","Dénomination","Forme juridique","Adresse du siège","Nom commercial","N° unique d'identification","Nom de naissance","Prénom"]
    CSV.open("datas.csv", "wb", headers: headers, write_headers: true) do |csv|
    end
  end

  def add_row_to_csv(values)
    CSV.open("datas.csv","a") do |csv|
      csv << values
    end
  end


  def label_doc_datas(page,region,fields)
    @values = {}
    @values["0-region"] = region
    @log_subscriber = ActiveSupport::LogSubscriber.new
    logger.debug @log_subscriber.send(:color, "---- CURRENT DOC ----", :blue)

    FIELDS.each_with_index do |potential_field,index|
      @label = page.search("label").text_includes(potential_field)
      @text = @label.first ? @label.first.text : nil

      fields.each do |parsed_field|
        if @text
          if @text == parsed_field.search("label").text
            label = sanitize_string(parsed_field.search("label").text)
            field_value = sanitize_string(parsed_field.search(".caim-doc-champ").text)
            @values["#{index+1}-#{label}"] = field_value
          end
        else
          @values["#{index+1}-unknowed"] = "null"
        end
      end

    end

    logger.debug @log_subscriber.send(:color, "#{@values}", :green)
    self.add_row_to_csv(@values.values)
  end


  def get_doc_datas_with_label(fields,region)
    @values = {}
    @values["0-region"] = region
    @log_subscriber = ActiveSupport::LogSubscriber.new
    logger.debug @log_subscriber.send(:color, "---- CURRENT DOC ----", :blue)

    fields.each_with_index do |value,index|


      label = sanitize_string(value.search("label").text)
      logger.debug @log_subscriber.send(:color, "#{label.inspect}", :red)

      field_value = sanitize_string(value.search(".caim-doc-champ").text)

      logger.debug @log_subscriber.send(:color, "---- FIELD VALUE ----", :yellow)
      logger.debug @log_subscriber.send(:color, "#{field_value}", :yellow)


      if label != ""
        if field_value.match(/\d{5}/)
          logger.debug @log_subscriber.send(:color, "#{find_zipcode_in_string(field_value)}", :yellow)
          field_value = find_zipcode_in_string(field_value)
        end
        @values["#{index+1}-#{label}"] = field_value
      else
        if (index + 1) != 9
          @values["#{index+1}-#{label}"] = "null"
        else
          logger.debug @log_subscriber.send(:color, "Index == 9", :red, true)
        end
      end
    end

    logger.debug @log_subscriber.send(:color, "#{@values}", :green)
    self.add_row_to_csv(@values.values)
  end


  def pagination_next_link(page,region)
    @pagination_link = page.link_with(text: "Suivant")
    @item_links = self.item_links(page)

    logger.debug ActiveSupport::LogSubscriber.new.send(:color, "---- Item links ----", :cyan)
    puts @item_links.inspect
    puts "--------------------"

    if @item_links.size != 0
      @item_links.each do |item_link|
        @show_page = item_link.click
        # @values = self.get_doc_datas_with_label(self.item_values(@show_page),region)
        self.label_doc_datas(@show_page,region,self.item_values(@show_page))
      end
    end

    if @pagination_link
      puts @pagination_link.inspect
      @new_page = @pagination_link.click
      self.pagination_next_link(@new_page,region)
    else
      return
    end
  end

  def item_values(show_page)
    show_page.search(self.doc_raw_dom_id)
  end

  def item_links(result_page)
    result_page.links.select { |l| l if l.text.include?(self.item_link_scrapping_name) }
  end

  def choose_select_option(form,number)
    form.field_with(name: self.form_search_field_name).options[number].select
  end

  def select_option_value(form,number)
    form.field_with(name: self.form_search_field_name).options[number].text
  end

  def submit_form(agent,form)
    form_submit_redirection_page = agent.submit(form)
    return form_submit_redirection_page
  end

  def start_page
    a = Mechanize.new { |agent| agent.user_agent_alias = 'Mac Safari' }
    if !self.ssl?
      a.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    end
    page = a.get(self.url)
    return {page: page, agent: a}
  end

  def find_form(page)
    form = page.form_with(id: self.form_search_id)
    select_size = form.field_with(name: self.form_search_field_name).options.size
    if select_size
      self.form_select_options_size = select_size
      self.save
    end
    return form
  end

  def ssl?
    self.ssl
  end

end
