class Admin
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

  # Devise field
  field :email, type: String, default: ""
  field :encrypted_password, type: String, default: ""
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time
  field :remember_created_at, type: Time
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  # Classic fields
  field :firstname, type: String
  field :lastname, type: String
  field :phone, type: String
  field :role, type: Integer

  def role
    values = {1 => "base", 2 => "admin", 3 => "leader"}
    if self[:role]
      value = values[self[:role].to_i]
    else
      value = nil
    end
    return value
  end

  def role_color
    if self[:role]
      if self[:role] == 1
        value = "blue"
      elsif self[:role] == 2
        value = "green"
      elsif self[:role] == 3
        value = "red"
      end
    else
      value = nil
    end
    return value
  end

end
