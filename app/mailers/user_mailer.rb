class UserMailer < BaseMandrillMailer
  include Resque::Mailer

  def admin_user_signup(email,user,company)
    subject = "Nouvelle inscription de l'utilisateur #{user[:firstname]} #{user[:lastname]}"
    merge_vars = {
      "FIRST_NAME" => user[:firstname],
      "LAST_NAME" => user[:lastname],
      "COMPANY" => company[:name],
      "CITY" => company[:city],
      "AGENCY" => company[:name],
      "PHONE" => user[:phone],
      "EMAIL" => user[:email],
      "COMPANY_TYPE" => company[:juridical_nature],
      "SIREN" => company[:siren],
    }
    body = mandrill_template("ed-notif-inscription", merge_vars)
    from = "Edmond <webadmin@edmond.immo>"
    replyto = "webadmin@edmond.immo"
    send_mail(email, subject, body, from, replyto)
  end

  def subs_reminder(user,sub,days)
    puts "---- Inside Subs reminder method ----"
    subject = "Votre abonnement #{sub[:name]} se termine dans #{days} jours"
    sub_end_date = I18n.localize(sub[:end_date], format: :french)
    merge_vars = {
      "FIRST_NAME" => user[:firstname],
      "END_DATE" => sub[:end_date],
      "SUBSCRIPTION" => sub[:name],
      "SUBSCRIPTION_CATEGORY" => sub[:category],
      "COST_WT" => sub[:int_amount],
      "FREQUENCY" => sub[:recurrence],
      "DAY_OVER" => days,
    }
    body = mandrill_template("ed-reminder-abo", merge_vars)
    from = "Edmond <webadmin@edmond.immo>"
    replyto = "webadmin@edmond.immo"
    send_mail(user[:email], subject, body, from, replyto)
  end

  def financing_request(user)
    puts "---- Inside user mailer with resque and mandrill API ----"
    subject = "On avance sur votre avance :-)"
    merge_vars = {
      "FIRST_NAME" => user[:firstname],
    }
    body = mandrill_template("ed-conf-financement", merge_vars)
    from = "Edmond <webadmin@edmond.immo>"
    replyto = "webadmin@edmond.immo"
    send_mail(user[:email], subject, body, from, replyto)
  end

  def user_question(user,question,company,email)
    puts "---- Inside user question ----"
    subject = "Nouveau message depuis l'espace personnel"
    merge_vars = {
      "FIRST_NAME" => user[:firstname],
      "LAST_NAME" => user[:lastname],
      "PHONE" => user[:phone],
      "EMAIL" => user[:email],
      "MESSAGE" => question[:message],
      "COMPANY" => company[:name],
      "CITY" => company[:city],
      "AGENCY" => company[:trade_name],
      "COMPANY_TYPE" => company[:juridical_nature],
      "SIREN" => company[:siren],
    }
    body = mandrill_template("ed-notif-message", merge_vars)
    from = "Edmond <webadmin@edmond.immo>"
    replyto = "webadmin@edmond.immo"
    send_mail(email, subject, body, from, replyto)
  end

  def admin_financing_request(user,company,request,email)
    puts "---- Nouvelle demande d'etude ----"
    subject = "Nouvelle demande d'étude"
    siren = company[:siren] ? company[:siren] : "Non renseigné"
    merge_vars = {
      "FIRST_NAME" => user[:firstname],
      "LAST_NAME" => user[:lastname],
      "PHONE" => user[:phone],
      "EMAIL" => user[:email],
      "AGENCY" => company[:trade_name],
      "CITY" => company[:city],
      "SIREN" => siren,
      "COMPANY_TYPE" => company[:juridical_nature],
      "COMPANY" => company[:name],
      "NB_TRANSACTIONS" => request[:year_transactions_count],
      "CA_TRANSACTIONS" => request[:annual_revenue],
      "NB_COMPROMIS" => request[:pending_transactions_count],
      "CA_HONORAIRES" => request[:total_transactions_amount],
    }
    body = mandrill_template("ed-notif-demande", merge_vars)
    from = "Edmond[Admin] <webadmin@edmond.immo>"
    replyto = "webadmin@edmond.immo"
    send_mail(email, subject, body, from, replyto)
  end

  def signup_success(user)
    puts "---- Signup success ----"
    subject = "Bienvenue chez Edmond !"
    merge_vars = {
      "FIRST_NAME" => user[:firstname],
      "EMAIL" => user[:email],
      "DASHBOARD" => "https://app.edmond.immo/dashboard/start",
    }
    body = mandrill_template("ed-inscription-edmond", merge_vars)
    from = "Edmond <webadmin@edmond.immo>"
    replyto = "webadmin@edmond.immo"
    send_mail(user[:email], subject, body, from, replyto)
  end

  def today_signed_compromise(user,compromise)
    puts "---- Today signed compromise ----"
    id = compromise["_id"]["$oid"]
    subject = "Alors, c'est signé ?"
    merge_vars = {
      "FIRST_NAME" => user[:firstname],
      "VENDOR_NAME" => compromise[:seller_lastname],
      "VALID_VENTE" => "https://app.edmond.immo/dashboard/compromises/validation/#{id}",
    }
    body = mandrill_template("ed-valid-vente", merge_vars)
    from = "Edmond <webadmin@edmond.immo>"
    replyto = "webadmin@edmond.immo"
    send_mail(user[:email], subject, body, from, replyto)
  end

end
