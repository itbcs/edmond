require "mandrill"
require "open-uri"
require "base64"

class BaseMandrillMailer < ActionMailer::Base

  private

  def send_mail(email, subject, body, from, replyto)
    mail(from: from, reply_to: replyto, to: email, subject: subject, body: body, content_type: "text/html")
  end

  def mandrill_template(template_name, attributes)
    mandrill = Mandrill::API.new("bvTrr00fZCDYrdPFaYwVag")
    merge_vars = attributes.map do |key, value|
      { name: key, content: value }
    end
    mandrill.templates.render(template_name, [], merge_vars)["html"]
  end

end
