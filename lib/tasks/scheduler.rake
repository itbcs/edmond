desc "This task is called by the Heroku scheduler add-on"

task :today_signature_date => :environment do
  puts "Inside today signature date scheduler action"
  @users = User.all
  @users.each do |user|
    if user.companies?
      @company = user.companies.last
      if @company.compromises?
        @company.compromises.each do |compromise|
          if compromise.authentic_act_signature_date == Date.today
            puts "This compromise is normally signed today"
            UserMailer.today_signed_compromise(user,compromise).deliver
          else
            puts "This compromise is not scheduled to be signed today"
          end
        end
      end
    end
  end
end


task :today_subs_notifications => :environment do
  puts "Inside today subs notifications"
  SubscriptionsNotifications.call
end
