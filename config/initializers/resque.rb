# frozen_string_literal: true
require 'resque/server'

ENV['REDISTOGO_URL'] ||= 'redis://username:password@host:1234/'

uri = URI.parse("redis://redistogo:c4386b623d68bbdebb01385c07dd7a9f@porgy.redistogo.com:11777/")
Resque.redis = Redis.new(host: uri.host, port: uri.port, password: uri.password, thread_safe: true)

Dir['/app/app/jobs/*.rb'].each { |file| require file }
