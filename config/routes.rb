Rails.application.routes.draw do
  mount Resque::Server.new, at: "/resque"
  root to: 'dashboard/pages#start'
  devise_for :users, controllers: { sessions: 'users/sessions', registrations: 'users/registrations' }, path_names: { sign_in: 'connexion', sign_up: "inscription" }
  devise_for :admins, controllers: { sessions: 'admins/sessions', registrations: 'admins/registrations' }, path_names: { sign_in: 'connexion', sign_up: "inscription" }

  get "scrapper", to: "pages#scrapper", as: "scrapper"
  get "user_to_csv", to: "pages#generate_users_csv_infos", as: "user_csv"

  namespace :widgets do
    get ':template', to: 'pages#simulation', as: "simulation"
  end

  namespace :dashboard do
    get "start", to: "pages#start", as: "start"
    get "synthesis", to: "pages#synthesis", as: "synthesis"
    get "restricted_synthesis", to: "pages#restricted_synthesis", as: "restricted_synthesis"
    get "management", to: "pages#management", as: "management"
    get "sells", to: "pages#sells", as: "sells"
    get "costs", to: "pages#costs", as: "costs"
    get "treasury", to: "pages#treasury", as: "treasury"
    resources :companies
    resources :compromises
    resources :costs
    resources :subscriptions
    resources :subscription_types
    resources :subscription_items
    resources :users, only: [:edit, :update]
    resources :user_questions, only: [:new, :create]
    resources :financing_requests, only: [:new, :create]
    get "subscriptions/dynamic_field/:field", to: "subscriptions#dynamic_field", as: "dynamic_field"
    get "subscriptions/close/:id", to: "subscriptions#close", as: "subscription_close"
    get "subscriptions/renew/:id", to: "subscriptions#renew", as: "subscription_renew"

    get "financing_request_success", to: "financing_requests#success", as: "financing_request_success"
    get "new_pending", to: "compromises#new_pending", as: "new_pending"
    get "compromises/validation/:id", to: "compromises#validation", as: "validation"
    post "compromises/signed/:id", to: "compromises#signed", as: "compromise_signed"
    post "compromises/change_signed_date/:id", to: "compromises#change_signed_date", as: "compromise_change_signed_date"
    post "compromises/update_act_signature_date/:id", to: "compromises#update_act_signature_date", as: "compromise_update_act_signature_date"

    post "month-costs", to: "costs#month_costs", as: "month_costs"
    post "year-costs", to: "costs#year_costs", as: "year_costs"
    get "polls/sad", to: "polls#sad"
    get "polls/mitigate", to: "polls#mitigate"
    get "polls/happy", to: "polls#happy"

    get "account/subscription", to: "users#infos", as: "subscription_infos"
    get "account/subscription/success", to: "users#subscription_success", as: "subscription_success"
    post "account/subscription/cancel", to: "users#stripe_subscription_cancel", as: "subscription_cancel"
    post "acccount/subscription/coupon", to: "users#coupon_code", as: "coupon_code"

  end

  namespace :admin do
    resources :users
    resources :user_scorings
    resources :companies
    resources :compromises
    resources :compromise_scorings
    resources :financing_requests
    resources :admins
    get "start",  to: "pages#start", as: "start"
    get "test_api_call", to: "payments#test_api_call", as: "test_payment"
    post "payment_data", to: "payments#payment_data", as: "payment_data"
    get "payments/success", to: "payments#success", as: "payment_success"
    namespace :webhooks do
      namespace :payments do
        post "checkout/completed", to: "checkouts#completed", as: "checkout_completed"
        post "subscription/trial_will_end", to: "subscriptions#trial_will_end", as: "subscription_trial_will_end"
        post "subscription/updated", to: "subscriptions#updated", as: "subscription_updated"
        post "subscription/deleted", to: "subscriptions#deleted", as: "subscription_deleted"
        post "customer/updated", to: "customers#updated", as: "customer_updated"
      end
    end
  end

  namespace :wizard do
    resources :users
    get "success", to: "users#success", as: "success"
  end

  get "/404", to: "errors#not_found", as: "notfound"
  get "/500", to: "errors#internal_error", as: "internal_error"
  get '*unmatched_route', to: 'application#not_found_redirect'

end
