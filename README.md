# EDMOND

Features:

* Fully Ajax navigation

* Management tools for free

* Mongoid config

* Custom fields

* Image Direct Upload (From browser)

* Resque on all job queues

* Optimized db queries (Eager loading for main resources)

* Deployment pipeline via codeship
